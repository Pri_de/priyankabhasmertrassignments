//
//  MyView.m
//  Window
//
//  Created by Yogesh Deshmukh on 04/08/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>
#import "vmath.h"

#import "GLESView.h"
#import "Sphere.h"

// for sphere
GLfloat lightAmbient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

enum
{
    X_AXIS_ROT,
    Y_AXIS_ROT,
    Z_AXIS_ROT
};

typedef struct
{
    GLfloat ambient[4];
    GLfloat diffused[4];
    GLfloat specular[4];
    GLfloat shininess;
}Material_Properties_Struct_type;


GLuint kaUniform, kdUniform, ksUniform, materialShininessUniform;

Material_Properties_Struct_type material_Properties_st[] =
{
    // ***** 1st sphere on 1st column, emerald *****
    0.0215,0.1745,0.0215,1.0f,//Ambient
    0.0768,0.61424,0.07568,1.0f,//Diffused
    0.633,0.727811,0.633,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 2nd sphere on 1st column, jade *****
    0.135 ,0.2225 ,0.1575,1.0f,//Ambient
    0.54,0.89,0.63 ,1.0f,//Diffused
    0.316228,0.316228,0.316228,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 3rd sphere on 1st column obsidian *****
    0.05375 ,0.05 ,0.06625,1.0f,//Ambient
    0.18275,0.17 ,0.22525,1.0f,//Diffused
    0.332741,0.328634,0.346435,1.0f,//Specular
    0.3 * 128,//Shininess
    
    // ***** 4th sphere on 1st column pearl *****
    0.25 ,0.20725 ,0.20725 ,1.0f,//Ambient
    1.0 ,0.829,0.829,1.0f,//Diffused
    0.296648,0.296648,0.296648,1.0f,//Specular
    0.088 * 128,//Shininess
    
    // ***** 5th sphere on 1st column ruby *****
    0.1745 ,0.01175,0.01175,1.0f,//Ambient
    0.61424,0.04136,0.04136,1.0f,//Diffused
    0.727811,0.626959,0.626959,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 6th sphere on 1st column turquoise *****
    0.1 ,0.18725,0.1745,1.0f,//Ambient
    0.396,0.74151,0.69102,1.0f,//Diffused
    0.297254,0.30829,0.306678,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 1st sphere on 2nd column brass *****
    0.329412 ,0.223529,0.027451,1.0f,//Ambient
    0.780392,0.568627,0.113725,1.0f,//Diffused
    0.992157,0.941176,0.807843,1.0f,//Specular
    0.21794872 * 128,//Shininess
    
    // ***** 2nd sphere on 2nd column bronze *****
    0.2125 ,0.1275 ,0.054 ,1.0f,//Ambient
    0.714,0.4284 ,0.18144,1.0f,//Diffused
    0.393548,0.271906,0.166721,1.0f,//Specular
    0.2 * 128,//Shininess
    
    // ***** 3rd sphere on 2nd column chrome *****
    0.25,0.25,0.25,1.0f,//Ambient
    0.4,0.4,0.4,1.0f,//Diffused
    0.774597,0.774597,0.774597,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 4th sphere on 2nd column copper *****
    0.19125 ,0.0735 ,0.0225 ,1.0f,//Ambient
    0.7038,0.27048 ,0.0828 ,1.0f,//Diffused
    0.256777,0.137622,0.086014,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 5th sphere on 2nd column gold *****
    0.24725,0.1995 ,0.0745,1.0f,//Ambient
    0.75164,0.60648,0.22648,1.0f,//Diffused
    0.628281,0.555802,0.366065,1.0f,//Specular
    0.4 * 128,//Shininess
    
    // ***** 6th sphere on 2nd column silver *****
    0.19225 ,0.19225 ,0.19225 ,1.0f,//Ambient
    0.50754,0.50754,0.50754,1.0f,//Diffused
    0.508273,0.508273,0.508273,1.0f,//Specular
    0.4 * 128,//Shininess
    
    // ***** 1st sphere on 3rd column black *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.0,0.0,0.0,1.0f,//Diffused
    0.50 ,0.50 ,0.50 ,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 2nd sphere on 3rd column cyan *****
    0.0 ,0.1,0.06,1.0f,//Ambient
    0.0 ,0.50980392,0.50980392,1.0f,//Diffused
    0.50196078,0.50196078,0.50196078,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 3rd sphere on 2nd column green *****
    0.0 ,0.0 ,0.0 ,1.0f,//Ambient
    0.1,0.35,0.1,1.0f,//Diffused
    0.45,0.55 ,0.45,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 4th sphere on 3rd column red *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.5,0.0,0.0,1.0f,//Diffused
    0.7,0.6 ,0.6 ,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 5th sphere on 3rd column white *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.55 ,0.55 ,0.55 ,1.0f,//Diffused
    0.70, 0.70, 0.70,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 6th sphere on 3rd column yellow plastic *****
    0.0, 0.0, 0.0,1.0f,//Ambient
    0.5, 0.5, 0.0,1.0f,//Diffused
    0.60, 0.60, 0.50,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 1st sphere on 4th column black *****
    0.02, 0.02, 0.02,1.0f,//Ambient
    0.01, 0.01, 0.01,1.0f,//Diffused
    0.4, 0.4, 0.4,1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 2nd sphere on 4th column cyan *****
    0.0, 0.05, 0.05,1.0f,//Ambient
    0.4, 0.5, 0.5,1.0f,//Diffused
    0.04, 0.7, 0.7,1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 3rd sphere on 4th column green *****
    0.0, 0.05, 0.0, 1.0f,//Ambient
    0.4, 0.5, 0.4, 1.0f,//Diffused
    0.04, 0.7, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 4th sphere on 4th column red *****
    0.05, 0.0, 0.0, 1.0f,//Ambient
    0.5, 0.4, 0.4, 1.0f,//Diffused
    0.7, 0.04, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 5th sphere on 4th column white *****
    0.05, 0.05, 0.05, 1.0f,//Ambient
    0.5, 0.5, 0.5, 1.0f,//Diffused
    0.7, 0.7, 0.7, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 6th sphere on 4th column yellow rubber *****
    0.05, 0.05, 0.0, 1.0f,//Ambient
    0.5, 0.5, 0.4, 1.0f,//Diffused
    0.7, 0.7, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    //GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position;
    //GLuint mvpUniform;
    
    GLuint vbo_normals;
    GLuint modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    GLuint laUniform, ldUniform, lsUniform, lightPositionUniform;
    
    char gchar_rotation;
    //bool isLKeyPressed;
    bool animate, light;
    
    GLuint lKeyPressedUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    
    
}

- (id)initWithFrame:(CGRect)frame;
{
    
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            [self release];
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initialization
        isAnimating = NO;
        animationFrameInterval = 60;    // default since IOS 8.2
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled==1)" \
        "{" \
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
        "viewer_vector = -eye_coordinates.xyz;" \
        "}" \
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 transformed_normals;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "out vec4 FragColor;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 phong_ads_color;" \
        "if(u_lighting_enabled==1)" \
        "{" \
        "vec3 normalized_transformed_normals=normalize(transformed_normals);" \
        "vec3 normalized_light_direction=normalize(light_direction);" \
        "vec3 normalized_viewer_vector=normalize(viewer_vector);" \
        "vec3 ambient = u_La * u_Ka;" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "phong_ads_color=ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        //mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        // get uniform locations
        modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        laUniform = glGetUniformLocation(shaderProgramObject, "u_La");
        ldUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
        lsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
        kaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        // shininess of material ( value is conventionally between 1 to 200 )
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        
        lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");
        
        
        
        DrawSphere();
        
        // *** vertices, colors, shader attribs, vbo_position, vao initializations ***
        /*  const GLfloat triangleVertices[] =
         {  0.0f, 1.0f,  0.0f,
         -1.0f,-1.0f, 1.0f,
         1.0f, -1.0f, 1.0f,
         0.0f, 1.0f,  0.0f,
         1.0f, -1.0f, 1.0f,
         1.0f, -1.0f, -1.0f,
         0.0f, 1.0f,  0.0f,
         1.0f, -1.0f, -1.0f,
         -1.0f,-1.0f, -1.0f,
         0.0f, 1.0f,  0.0f,
         -1.0f,-1.0f, -1.0f,
         -1.0f,-1.0f, 1.0f
         };
         
         const GLfloat pyramidNormals[]=
         {
         0.5, 1.0, // front-top
         0.0, 0.0, // front-left
         1.0, 0.0, // front-right
         
         0.5, 1.0, // right-top
         1.0, 0.0, // right-left
         0.0, 0.0, // right-right
         
         0.5, 1.0, // back-top
         1.0, 0.0, // back-left
         0.0, 0.0, // back-right
         
         0.5, 1.0, // left-top
         0.0, 0.0, // left-left
         1.0, 0.0, // left-right
         };
         
         
         glGenVertexArrays(1, &vao_pyramid);
         glBindVertexArray(vao_pyramid);
         
         glGenBuffers(1, &vbo_position);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
         glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glGenBuffers(1, &vbo_pyramid_texture);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_texture);
         glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexcoords), pyramidTexcoords, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         glBindVertexArray(0);
         */
        
        // Square
        /*GLfloat cubeVertices[] =
         {
         0.75f, 0.75f, -0.75f, // top
         0.75f, 0.75f, 0.75f,
         -0.75f, 0.75f, 0.75f,
         -0.75f, 0.75f, -0.75f,
         
         0.75f, -0.75f, -0.75f, // bottom
         0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, -0.75f,
         
         0.75f, 0.75f, 0.75f, // front
         0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, 0.75f,
         -0.75f, 0.75f, 0.75f,
         
         -0.75f, 0.75f, -0.75f, // back
         -0.75f, -0.75f, -0.75f,
         0.75f, -0.75f, -0.75f,
         0.75f, 0.75f, -0.75f,
         
         0.75f, 0.75f, -0.75f, // right
         0.75f, -0.75f, -0.75f,
         0.75f, -0.75f, 0.75f,
         0.75f, 0.75f, 0.75f,
         
         -0.75f, 0.75f, 0.75f, // left
         -0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, -0.75f,
         -0.75f, 0.75f, -0.75f
         
         };
         
         const GLfloat cubeNormals[] =
         {
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f
         };
         
         glGenVertexArrays(1, &vao_cube);
         glBindVertexArray(vao_cube);
         
         glGenBuffers(1, &vbo_position);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
         glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glGenBuffers(1, &vbo_normals);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
         glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_DYNAMIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glBindVertexArray(0);
         printf("square setup loaded\n");
         */
        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        // enable texture
        glEnable(GL_TEXTURE_2D);
        
        
        // clear color
        glClearColor(0.0f,0.0f,0.0f,1.0f);    // black color
        
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        
        // GESTURE RECOGNITION
        // Tap Gesture Code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    printf("init finished.. now returning\n");
    
    return(self);
}

void Set_Material_Properties(Material_Properties_Struct_type material)
{
    glUniform3fv(kaUniform, 1, material.ambient);
    glUniform3fv(kdUniform, 1, material.diffused);
    glUniform3fv(ksUniform, 1, material.specular);
    glUniform1f(materialShininessUniform, material.shininess);
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    // code
    //static float anglePyramid=0.0f;
    //static float angleCube=0.0f;
    static GLfloat angle=0;
    printf("in draw \n");
    // Code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    //vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    
    if (light == true)
    {
        glUniform1i(lKeyPressedUniform, 1);
        
        // for light
        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        
    }
    else
    {
        glUniform1i(lKeyPressedUniform, 0);
    }
    
    if (gchar_rotation == X_AXIS_ROT)
    {
        lightPosition[0] = 0.0f;
        lightPosition[1] = 100 * cos(angle*M_PI / 180);
        lightPosition[2] = 100 * sin(angle*M_PI / 180);
        
        glUniform4fv(lightPositionUniform, 1, lightPosition);
    }
    else if (gchar_rotation == Y_AXIS_ROT)
    {
        lightPosition[1] = 0.0f;
        lightPosition[2] = 100 * cos(angle*M_PI / 180);
        lightPosition[0] = 100 * sin(angle*M_PI / 180);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        
    }
    else if (gchar_rotation == Z_AXIS_ROT)
    {
        lightPosition[0] = 100 * cos(angle*M_PI / 180);
        lightPosition[1] = 100 * sin(angle*M_PI / 180);
        lightPosition[2] = 0.0f;
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        
    }
    
    modelMatrix = vmath::translate(0.0f,0.0f, -4.0f);
    
    //rotationMatrix = vmath::rotate(angleCube, angleCube,angleCube);
    
    //modelViewMatrix = modelViewMatrix * rotationMatrix;
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    char x=0,y=0;
    char index_mat=0;
    float X=-7.0f,Y=5.0f,Z=-10.0f;
    
    for(y=0;y<4;y=y+1)
    {
        Y = Y - 2.0f;
        modelMatrix =vmath::translate(0.0f, -0.5f, 0.0f);
        for(x=0;x<6;x=x+1)
        {
            
            X= X+2.0f;
            modelMatrix =vmath::translate(X, Y, Z);
            glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
            Set_Material_Properties(material_Properties_st[index_mat++]);
            
            
            glBindVertexArray(vao);
            
            // draw
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
            glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
            
            
            // unbind vao
            glBindVertexArray(0);
            
            
        }
        
        X = -7.0f;
    }
    
    // stop using OpenGL program object
    glUseProgram(0);
    
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    // draw and stop
    //[self stopAnimation];
    angle = angle + 4.0f;
    // keep rotating
    //anglePyramid = anglePyramid + 1.0f;
    //angleCube = angleCube + 1.0f;
    
}

- (void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
    //glOrtho(left,right,bottom,top,near,far);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 1.0f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    gchar_rotation += 1;
    if(gchar_rotation >= 3)
    {
        gchar_rotation = 0;
    }
    
    /*if(isAnimating)
    {
        [self stopAnimation];
    }
    else*/
    {
        [self startAnimation];
    }
    
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(light == false)
    {
        light = true;
    }
    else
    {
        light = false;
    }
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void)dealloc
{
    
    // code
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    
    
    // destroy texture
    if(vbo_normals)
    {
        glDeleteTextures(1,&vbo_normals);
        vbo_normals=0;
    }
    
    // destroy vbo_position
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end

