//
//  MyView.m
//  Window
//
//  Created by Yogesh Deshmukh on 04/08/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>
#import "vmath.h"

#import "GLESView.h"
#import "Sphere.h"

// for sphere
GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuseBlue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightDiffuseGreen[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecularRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecularBlue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecularGreen[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightPositionRed[] = { 0.0f,0.0f,25.0f,1.0f };
GLfloat lightPositionBlue[] = { 25.0f,0.0f,0.0f,1.0f };
GLfloat lightPositionGreen[] = { 25.0f,0.0f,0.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;


@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    //GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position;
    //GLuint mvpUniform;
    
    GLuint vbo_normals;
    GLuint modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    GLuint laUniform, ldUniform, lsUniform, lightPositionUniform;
    GLuint kaUniform, kdUniform, ksUniform, materialShininessUniform;
    
    GLuint LdRed_uniform, LdBlue_uniform, LdGreen_uniform;
    GLuint LsRed_uniform, LsBlue_uniform, LsGreen_uniform;
    GLuint light_positionRed_uniform, light_positionBlue_uniform, light_positionGreen_uniform;
    
    
    GLuint PerFragmentPhong_uniform;
    bool gbPerFragmentPhong;
    //bool isLKeyPressed;
    bool animate, light;
    
    GLuint lKeyPressedUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    
    
}

- (id)initWithFrame:(CGRect)frame;
{
    
    self = [super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            [self release];
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initialization
        isAnimating = NO;
        animationFrameInterval = 60;    // default since IOS 8.2
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_positionRed;" \
        "uniform vec4 u_light_positionBlue;" \
        "uniform vec4 u_light_positionGreen;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_directionRed;" \
        "out vec3 light_directionGreen;" \
        "out vec3 light_directionBlue;" \
        "out vec3 viewer_vector;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_LdRed;" \
        "uniform vec3 u_LdBlue;" \
        "uniform vec3 u_LdGreen;" \
        "uniform vec3 u_LsRed;" \
        "uniform vec3 u_LsBlue;" \
        "uniform vec3 u_LsGreen;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform mediump int u_PerFragmentPhong;"\
        "out vec3 V_phong_ads_color;"\
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled==1)" \
        "{" \
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "light_directionRed = vec3(u_light_positionRed) - eye_coordinates.xyz;" \
        "light_directionBlue = vec3(u_light_positionBlue) - eye_coordinates.xyz;" \
        "light_directionGreen = vec3(u_light_positionGreen) - eye_coordinates.xyz;" \
        "viewer_vector = -eye_coordinates.xyz;" \
        "if(u_PerFragmentPhong==0)"\
        "{"\
        /*For red light*/
        "vec3 normalized_transformed_normals=normalize(transformed_normals);" \
        "vec3 normalized_light_direction=normalize(light_directionRed);" \
        "vec3 normalized_viewer_vector=normalize(viewer_vector);" \
        "vec3 ambient = u_La * u_Ka;" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "vec3 diffuse = u_LdRed * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "vec3 specular = u_LsRed * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "V_phong_ads_color=ambient + diffuse + specular;" \
        
        /*For Blue light*/
        "normalized_light_direction=normalize(light_directionBlue);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "diffuse = u_LdBlue * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "specular = u_LsBlue * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "V_phong_ads_color+=ambient + diffuse + specular;" \
        
        /*For Green Light*/
        "normalized_light_direction=normalize(light_directionGreen);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "diffuse = u_LdGreen * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "specular = u_LsGreen * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "V_phong_ads_color+=ambient + diffuse + specular;" \
        "}"\
        "}"\
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "out vec4 FragColor;" \
        "in vec3 transformed_normals;" \
        "in vec3 light_directionRed;" \
        "in vec3 light_directionBlue;" \
        "in vec3 light_directionGreen;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_LdRed;" \
        "uniform vec3 u_LdBlue;" \
        "uniform vec3 u_LdGreen;" \
        "uniform vec3 u_LsRed;" \
        "uniform vec3 u_LsBlue;" \
        "uniform vec3 u_LsGreen;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "uniform int u_PerFragmentPhong;"\
        "in vec3 V_phong_ads_color;"\
        "void main(void)" \
        "{" \
        "vec3 phong_ads_color;" \
        "if(u_lighting_enabled==1)" \
        "{" \
        "if(u_PerFragmentPhong==1)"\
        "{"\
        /*For red light*/
        "vec3 normalized_transformed_normals=normalize(transformed_normals);" \
        "vec3 normalized_light_direction=normalize(light_directionRed);" \
        "vec3 normalized_viewer_vector=normalize(viewer_vector);" \
        "vec3 ambient = u_La * u_Ka;" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "vec3 diffuse = u_LdRed * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "vec3 specular = u_LsRed * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "phong_ads_color=ambient + diffuse + specular;" \
        
        /*For Blue light*/
        "normalized_light_direction=normalize(light_directionBlue);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "diffuse = u_LdBlue * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "specular = u_LsBlue * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "phong_ads_color+=ambient + diffuse + specular;" \
        
        /*For Green Light*/
        "normalized_light_direction=normalize(light_directionGreen);" \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
        "diffuse = u_LdGreen * u_Kd * tn_dot_ld;" \
        "reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
        "specular = u_LsGreen * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "phong_ads_color+=ambient + diffuse + specular;" \
        "}" \
        "else"\
        "{"\
        "phong_ads_color = V_phong_ads_color;"\
        "}"\
        "}"\
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        //mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        // get uniform locations
        modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        laUniform = glGetUniformLocation(shaderProgramObject, "u_La");
        // diffuse color intensity of light
        LdRed_uniform = glGetUniformLocation(shaderProgramObject, "u_LdRed");
        LdBlue_uniform = glGetUniformLocation(shaderProgramObject, "u_LdBlue");
        LdGreen_uniform = glGetUniformLocation(shaderProgramObject, "u_LdGreen");
        // specular color intensity of light
        LsRed_uniform = glGetUniformLocation(shaderProgramObject, "u_LsRed");
        LsBlue_uniform = glGetUniformLocation(shaderProgramObject, "u_LsBlue");
        LsGreen_uniform = glGetUniformLocation(shaderProgramObject, "u_LsGreen");
        // position of light
        light_positionRed_uniform = glGetUniformLocation(shaderProgramObject, "u_light_positionRed");
        light_positionBlue_uniform = glGetUniformLocation(shaderProgramObject, "u_light_positionBlue");
        light_positionGreen_uniform = glGetUniformLocation(shaderProgramObject, "u_light_positionGreen");
        
        kaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        
        // shininess of material ( value is conventionally between 1 to 200 )
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        
        PerFragmentPhong_uniform = glGetUniformLocation(shaderProgramObject, "u_PerFragmentPhong");
        
        
        lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");
        
        
        
        DrawSphere();
        
        // *** vertices, colors, shader attribs, vbo_position, vao initializations ***
        /*  const GLfloat triangleVertices[] =
         {  0.0f, 1.0f,  0.0f,
         -1.0f,-1.0f, 1.0f,
         1.0f, -1.0f, 1.0f,
         0.0f, 1.0f,  0.0f,
         1.0f, -1.0f, 1.0f,
         1.0f, -1.0f, -1.0f,
         0.0f, 1.0f,  0.0f,
         1.0f, -1.0f, -1.0f,
         -1.0f,-1.0f, -1.0f,
         0.0f, 1.0f,  0.0f,
         -1.0f,-1.0f, -1.0f,
         -1.0f,-1.0f, 1.0f
         };
         
         const GLfloat pyramidNormals[]=
         {
         0.5, 1.0, // front-top
         0.0, 0.0, // front-left
         1.0, 0.0, // front-right
         
         0.5, 1.0, // right-top
         1.0, 0.0, // right-left
         0.0, 0.0, // right-right
         
         0.5, 1.0, // back-top
         1.0, 0.0, // back-left
         0.0, 0.0, // back-right
         
         0.5, 1.0, // left-top
         0.0, 0.0, // left-left
         1.0, 0.0, // left-right
         };
         
         
         glGenVertexArrays(1, &vao_pyramid);
         glBindVertexArray(vao_pyramid);
         
         glGenBuffers(1, &vbo_position);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
         glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glGenBuffers(1, &vbo_pyramid_texture);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_texture);
         glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexcoords), pyramidTexcoords, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         glBindVertexArray(0);
         */
        
        // Square
        /*GLfloat cubeVertices[] =
         {
         0.75f, 0.75f, -0.75f, // top
         0.75f, 0.75f, 0.75f,
         -0.75f, 0.75f, 0.75f,
         -0.75f, 0.75f, -0.75f,
         
         0.75f, -0.75f, -0.75f, // bottom
         0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, -0.75f,
         
         0.75f, 0.75f, 0.75f, // front
         0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, 0.75f,
         -0.75f, 0.75f, 0.75f,
         
         -0.75f, 0.75f, -0.75f, // back
         -0.75f, -0.75f, -0.75f,
         0.75f, -0.75f, -0.75f,
         0.75f, 0.75f, -0.75f,
         
         0.75f, 0.75f, -0.75f, // right
         0.75f, -0.75f, -0.75f,
         0.75f, -0.75f, 0.75f,
         0.75f, 0.75f, 0.75f,
         
         -0.75f, 0.75f, 0.75f, // left
         -0.75f, -0.75f, 0.75f,
         -0.75f, -0.75f, -0.75f,
         -0.75f, 0.75f, -0.75f
         
         };
         
         const GLfloat cubeNormals[] =
         {
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         0.0f, -1.0f, 0.0f,
         
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         0.0f, 0.0f, 1.0f,
         
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         -1.0f, 0.0f, 0.0f,
         
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f
         };
         
         glGenVertexArrays(1, &vao_cube);
         glBindVertexArray(vao_cube);
         
         glGenBuffers(1, &vbo_position);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
         glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glGenBuffers(1, &vbo_normals);
         glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
         glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_DYNAMIC_DRAW);
         
         glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
         
         glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
         
         glBindBuffer(GL_ARRAY_BUFFER, 0);
         
         glBindVertexArray(0);
         printf("square setup loaded\n");
         */
        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        // enable texture
        glEnable(GL_TEXTURE_2D);
        
        
        // clear color
        glClearColor(0.0f,0.0f,0.0f,1.0f);    // black color
        
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        
        // GESTURE RECOGNITION
        // Tap Gesture Code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    printf("init finished.. now returning\n");
    
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    // code
    //static float anglePyramid=0.0f;
    //static float angleCube=0.0f;
    //static GLfloat angle=0;;
    static float angle = 0;
    printf("in draw \n");
    // Code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    //vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
    
    if (light == true)
    {
        if (gbPerFragmentPhong == true)
        {
            glUniform1i(PerFragmentPhong_uniform, 1);
        }
        else
        {
            glUniform1i(PerFragmentPhong_uniform, 0);
        }
        
        glUniform1i(lKeyPressedUniform, 1);
        
        // for light
        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(LdRed_uniform, 1, lightDiffuseRed);
        glUniform3fv(LdBlue_uniform, 1, lightDiffuseBlue);
        glUniform3fv(LdGreen_uniform, 1, lightDiffuseGreen);
        glUniform3fv(LsRed_uniform, 1, lightSpecularRed);
        glUniform3fv(LsBlue_uniform, 1, lightSpecularBlue);
        glUniform3fv(LsGreen_uniform, 1, lightSpecularGreen);
        angle = angle + 1;
        //if (Set_color == RED)
        {
            lightPositionRed[1] = 10 * cos(angle*M_PI / 180.0);
            lightPositionRed[2] = 10 * sin(angle*M_PI / 180.0);
            
            glUniform4fv(light_positionRed_uniform, 1, lightPositionRed);
        }
        //if (Set_color == BLUE)
        {
            lightPositionBlue[0] = 10 * cos(angle*M_PI / 180.0);
            lightPositionBlue[1] = 10 * sin(angle*M_PI / 180.0);
            glUniform4fv(light_positionBlue_uniform, 1, lightPositionBlue);
        }
        //if (Set_color == GREEN)
        {
            lightPositionGreen[2] = 10 * cos(angle*M_PI / 180.0);
            lightPositionGreen[0] = 10 * sin(angle*M_PI / 180.0);
            glUniform4fv(light_positionGreen_uniform, 1, lightPositionGreen);
        }
        // for material
        glUniform3fv(kaUniform, 1, material_ambient);
        glUniform3fv(kdUniform, 1, material_diffuse);
        glUniform3fv(ksUniform, 1, material_specular);
        glUniform1f(materialShininessUniform, material_shininess);

        
    }
    else
    {
        glUniform1i(lKeyPressedUniform, 0);
    }
    
    
    modelMatrix = vmath::translate(0.0f,0.0f, -4.0f);
    
    //rotationMatrix = vmath::rotate(angleCube, angleCube,angleCube);
    
    //modelViewMatrix = modelViewMatrix * rotationMatrix;
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    
    // *** bind vao ***
    glBindVertexArray(vao);
    
    
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    
    // *** unbind vao ***
    glBindVertexArray(0);
    
    // stop using OpenGL program object
    glUseProgram(0);
    
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    // draw and stop
    //[self stopAnimation];
    angle = angle + 1.0f;
    // keep rotating
    //anglePyramid = anglePyramid + 1.0f;
    //angleCube = angleCube + 1.0f;
    
}

- (void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
    //glOrtho(left,right,bottom,top,near,far);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 1.0f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

- (void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
    /*if(isAnimating)
    {
        [self stopAnimation];
    }
    else*/
    {
        [self startAnimation];
    }
    
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(light == false)
    {
        light = true;
    }
    else
    {
        light = false;
    }
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void)dealloc
{
    
    // code
    if (vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
    
    
    
    // destroy texture
    if(vbo_normals)
    {
        glDeleteTextures(1,&vbo_normals);
        vbo_normals=0;
    }
    
    // destroy vbo_position
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }
    
    // destroy vbo
    if (vbo_index)
    {
        glDeleteBuffers(1, &vbo_index);
        vbo_index = 0;
    }
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end

