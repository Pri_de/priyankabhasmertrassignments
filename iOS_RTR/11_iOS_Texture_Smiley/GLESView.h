//
//  MyView.h
//  Window
//
//  Created by Yogesh Deshmukh on 04/08/18.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

-(void)startAnimation;
-(void)stopAnimation;

@end

