//
//  AppDelegate.h
//  Window
//
//  Created by Yogesh Deshmukh on 04/08/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

