//
//  GLESView.h
//  Window
//
//  Created by ASTROMEDICOMP on 28/05/18.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

-(void)startAnimation;
-(void)stopAnimation;

@end
