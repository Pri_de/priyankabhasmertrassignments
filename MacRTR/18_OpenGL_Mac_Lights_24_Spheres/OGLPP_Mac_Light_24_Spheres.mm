#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import <math.h>
#import "vmath.h"
#import "Sphere.h"

// declared in Sphere.h
/*enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};*/

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

// global variables
FILE *gpFile=NULL;

// for sphere
GLfloat lightAmbient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };


enum
{
    X_AXIS_ROT,
    Y_AXIS_ROT,
    Z_AXIS_ROT
};

typedef struct
{
    GLfloat ambient[4];
    GLfloat diffused[4];
    GLfloat specular[4];
    GLfloat shininess;
}Material_Properties_Struct_type;


GLuint kaUniform, kdUniform, ksUniform, materialShininessUniform;

Material_Properties_Struct_type material_Properties_st[] =
{
    // ***** 1st sphere on 1st column, emerald *****
    0.0215,0.1745,0.0215,1.0f,//Ambient
    0.0768,0.61424,0.07568,1.0f,//Diffused
    0.633,0.727811,0.633,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 2nd sphere on 1st column, jade *****
    0.135 ,0.2225 ,0.1575,1.0f,//Ambient
    0.54,0.89,0.63 ,1.0f,//Diffused
    0.316228,0.316228,0.316228,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 3rd sphere on 1st column obsidian *****
    0.05375 ,0.05 ,0.06625,1.0f,//Ambient
    0.18275,0.17 ,0.22525,1.0f,//Diffused
    0.332741,0.328634,0.346435,1.0f,//Specular
    0.3 * 128,//Shininess
    
    // ***** 4th sphere on 1st column pearl *****
    0.25 ,0.20725 ,0.20725 ,1.0f,//Ambient
    1.0 ,0.829,0.829,1.0f,//Diffused
    0.296648,0.296648,0.296648,1.0f,//Specular
    0.088 * 128,//Shininess
    
    // ***** 5th sphere on 1st column ruby *****
    0.1745 ,0.01175,0.01175,1.0f,//Ambient
    0.61424,0.04136,0.04136,1.0f,//Diffused
    0.727811,0.626959,0.626959,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 6th sphere on 1st column turquoise *****
    0.1 ,0.18725,0.1745,1.0f,//Ambient
    0.396,0.74151,0.69102,1.0f,//Diffused
    0.297254,0.30829,0.306678,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 1st sphere on 2nd column brass *****
    0.329412 ,0.223529,0.027451,1.0f,//Ambient
    0.780392,0.568627,0.113725,1.0f,//Diffused
    0.992157,0.941176,0.807843,1.0f,//Specular
    0.21794872 * 128,//Shininess
    
    // ***** 2nd sphere on 2nd column bronze *****
    0.2125 ,0.1275 ,0.054 ,1.0f,//Ambient
    0.714,0.4284 ,0.18144,1.0f,//Diffused
    0.393548,0.271906,0.166721,1.0f,//Specular
    0.2 * 128,//Shininess
    
    // ***** 3rd sphere on 2nd column chrome *****
    0.25,0.25,0.25,1.0f,//Ambient
    0.4,0.4,0.4,1.0f,//Diffused
    0.774597,0.774597,0.774597,1.0f,//Specular
    0.6 * 128,//Shininess
    
    // ***** 4th sphere on 2nd column copper *****
    0.19125 ,0.0735 ,0.0225 ,1.0f,//Ambient
    0.7038,0.27048 ,0.0828 ,1.0f,//Diffused
    0.256777,0.137622,0.086014,1.0f,//Specular
    0.1 * 128,//Shininess
    
    // ***** 5th sphere on 2nd column gold *****
    0.24725,0.1995 ,0.0745,1.0f,//Ambient
    0.75164,0.60648,0.22648,1.0f,//Diffused
    0.628281,0.555802,0.366065,1.0f,//Specular
    0.4 * 128,//Shininess
    
    // ***** 6th sphere on 2nd column silver *****
    0.19225 ,0.19225 ,0.19225 ,1.0f,//Ambient
    0.50754,0.50754,0.50754,1.0f,//Diffused
    0.508273,0.508273,0.508273,1.0f,//Specular
    0.4 * 128,//Shininess
    
    // ***** 1st sphere on 3rd column black *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.0,0.0,0.0,1.0f,//Diffused
    0.50 ,0.50 ,0.50 ,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 2nd sphere on 3rd column cyan *****
    0.0 ,0.1,0.06,1.0f,//Ambient
    0.0 ,0.50980392,0.50980392,1.0f,//Diffused
    0.50196078,0.50196078,0.50196078,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 3rd sphere on 2nd column green *****
    0.0 ,0.0 ,0.0 ,1.0f,//Ambient
    0.1,0.35,0.1,1.0f,//Diffused
    0.45,0.55 ,0.45,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 4th sphere on 3rd column red *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.5,0.0,0.0,1.0f,//Diffused
    0.7,0.6 ,0.6 ,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 5th sphere on 3rd column white *****
    0.0,0.0,0.0,1.0f,//Ambient
    0.55 ,0.55 ,0.55 ,1.0f,//Diffused
    0.70, 0.70, 0.70,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 6th sphere on 3rd column yellow plastic *****
    0.0, 0.0, 0.0,1.0f,//Ambient
    0.5, 0.5, 0.0,1.0f,//Diffused
    0.60, 0.60, 0.50,1.0f,//Specular
    0.25 * 128,//Shininess
    
    // ***** 1st sphere on 4th column black *****
    0.02, 0.02, 0.02,1.0f,//Ambient
    0.01, 0.01, 0.01,1.0f,//Diffused
    0.4, 0.4, 0.4,1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 2nd sphere on 4th column cyan *****
    0.0, 0.05, 0.05,1.0f,//Ambient
    0.4, 0.5, 0.5,1.0f,//Diffused
    0.04, 0.7, 0.7,1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 3rd sphere on 4th column green *****
    0.0, 0.05, 0.0, 1.0f,//Ambient
    0.4, 0.5, 0.4, 1.0f,//Diffused
    0.04, 0.7, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 4th sphere on 4th column red *****
    0.05, 0.0, 0.0, 1.0f,//Ambient
    0.5, 0.4, 0.4, 1.0f,//Diffused
    0.7, 0.04, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 5th sphere on 4th column white *****
    0.05, 0.05, 0.05, 1.0f,//Ambient
    0.5, 0.5, 0.5, 1.0f,//Diffused
    0.7, 0.7, 0.7, 1.0f,//Specular
    0.078125 * 128,//Shininess
    
    // ***** 6th sphere on 4th column yellow rubber *****
    0.05, 0.05, 0.0, 1.0f,//Ambient
    0.5, 0.5, 0.4, 1.0f,//Diffused
    0.7, 0.7, 0.04, 1.0f,//Specular
    0.078125 * 128,//Shininess
};



// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point func
int main(int argc,const char * argv[])
{

	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
	
}

// interface implementation
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	
	//code
	// log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
	
	// code// window
	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);
	
	// create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect
									styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									backing:NSBackingStoreBuffered
									defer:NO];

	[window setTitle:@"macOS OGLPP"];
	[window center];
	
	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
	
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	// code
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}


- (void)dealloc
{
	//code
	[glView release];
	
	[window release];
	
	[super dealloc];
	
}
@end

@implementation GLView
{
	@private
    CVDisplayLinkRef displayLink;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_vertex;
	GLuint vbo_normals;
	
	GLfloat angleCube;

    GLuint mvpUniform;
    
    // for lights
    GLuint modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    GLuint laUniform, ldUniform, lsUniform, lightPositionUniform;
    
    char gchar_rotation;
    
    bool isLKeyPressed;//, isAKeyPressed;
    bool bLight;
    
    GLuint lKeyPressedUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    
    // for sphere
  
}

- (id)initWithFrame:(NSRect)frame; // confirm the semicolon
{

	//code
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            // Sir gave us 4_1 core profile code but on Manisha's MacBook 3_2 profile support was available
            // so here onwards core profile has been changed to 3_2 and shader version to 330
            // It does not affect much to the shaders since we are not using any built in function from latest shader versions
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion3_2Core,
            // Specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0}; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting ...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version  : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version    : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   // *** VERTEX SHADER ***
    // create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform vec4 u_light_position;" \
    "uniform int u_lighting_enabled;" \
    "out vec3 transformed_normals;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_lighting_enabled==1)" \
    "{" \
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
    "light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
    "viewer_vector = -eye_coordinates.xyz;" \
    "}" \
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // re-initialize
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec3 transformed_normals;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    "out vec4 FragColor;" \
    "uniform vec3 u_La;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Ls;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "uniform int u_lighting_enabled;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color;" \
    "if(u_lighting_enabled==1)" \
    "{" \
    "vec3 normalized_transformed_normals=normalize(transformed_normals);" \
    "vec3 normalized_light_direction=normalize(light_direction);" \
    "vec3 normalized_viewer_vector=normalize(viewer_vector);" \
    "vec3 ambient = u_La * u_Ka;" \
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
    "phong_ads_color=ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color, 1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    shaderProgramObject = glCreateProgram();
    
    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    // attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // get uniform locations
    modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
    viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
    projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
    

    laUniform = glGetUniformLocation(shaderProgramObject, "u_La");
    ldUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
    lsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
    kaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
    kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
    ksUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
    
    // L/l key is pressed or not
    lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

    DrawSphere();
    
    glClearDepth(1.0f);
    // enable depth testing
    glEnable(GL_DEPTH_TEST);
    // depth test to do
    glDepthFunc(GL_LEQUAL);
    // We will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);  ignored only for this assignment
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
    
    // set projection matrix to identity matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

void Set_Material_Properties(Material_Properties_Struct_type material)
{
    glUniform3fv(kaUniform, 1, material.ambient);
    glUniform3fv(kdUniform, 1, material.diffused);
    glUniform3fv(ksUniform, 1, material.specular);
    glUniform1f(materialShininessUniform, material.shininess);
}



-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
        height=1;
	
	
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
    //glOrtho(left,right,bottom,top,near,far);
    perspectiveProjectionMatrix = vmath::perspective(45, ((GLfloat)width / (GLfloat)height), 0.1, 100);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}


- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];	
}

- (void)drawView
{
    
    static float angle;
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
 
    
    if (bLight == true)
    {
        glUniform1i(lKeyPressedUniform, 1);
        
        // for light
        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        glUniform4fv(lightPositionUniform, 1, lightPosition);

    }
    else
    {
        glUniform1i(lKeyPressedUniform, 0);
    }
    
    
    angle = angle + 1.0f;
    if (gchar_rotation == X_AXIS_ROT)
    {
        lightPosition[0] = 0.0f;
        lightPosition[1] = 100 * cos(angle*M_PI / 180);
        lightPosition[2] = 100 * sin(angle*M_PI / 180);
        
        glUniform4fv(lightPositionUniform, 1, lightPosition);
    }
    else if (gchar_rotation == Y_AXIS_ROT)
    {
        lightPosition[1] = 0.0f;
        lightPosition[2] = 100 * cos(angle*M_PI / 180);
        lightPosition[0] = 100 * sin(angle*M_PI / 180);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        
    }
    else if (gchar_rotation == Z_AXIS_ROT)
    {
        lightPosition[0] = 100 * cos(angle*M_PI / 180);
        lightPosition[1] = 100 * sin(angle*M_PI / 180);
        lightPosition[2] = 0.0f;
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        
    }
    
    
    // push square deeper-right for perspective
	modelMatrix = vmath::translate(0.0f,0.0f,-4.0f);
	
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    
    char x=0,y=0;
    char index_mat=0;
    float X=-7.0f,Y=5.0f,Z=-10.0f;
    
    for(y=0;y<4;y=y+1)
    {
        Y = Y - 2.0f;
        modelMatrix =vmath::translate(0.0f, -0.5f, 0.0f);
        for(x=0;x<6;x=x+1)
        {
            
            X= X+2.0f;
            modelMatrix =vmath::translate(X, Y, Z);
            glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
            Set_Material_Properties(material_Properties_st[index_mat++]);
            
            
            glBindVertexArray(vao);
            
            // draw
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
            glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
            
            
            // unbind vao
            glBindVertexArray(0);
            
            
        }
        
        X = -7.0f;
    }
    
    // stop using OpenGL program object
    glUseProgram(0);

    /*if(animate == true)
    {
        [self update];
    }*/
	
	
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


- (void)update
{
	angleCube += 1.0f;
	if(angleCube >= 360.0f)
	{
		angleCube = 0.0f;
	}
/*
	anglePyramid += 0.5f;
	if(anglePyramid >= 360.0f)
	{
		anglePyramid = 0.0f;
	}*/
}

- (BOOL)acceptsFirstResponder
{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: //Esc
			[self release];
			[NSApp terminate:self];
			break;
		
		case 'F':
		case 'f':
			//centralText=@"'F' or 'f' Key Is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
        
        
        case 'L': // for 'L' or 'l'
        case 'l':
            if (isLKeyPressed == false)
            {
                bLight = true;
                isLKeyPressed = true;
            }
            else
            {
                bLight = false;
                isLKeyPressed = false;
            }
            break;
            
        case 'X':
        case 'x':
            gchar_rotation = X_AXIS_ROT;
            break;
        case 'Y':
        case 'y':
            gchar_rotation = Y_AXIS_ROT;
            break;
        case 'Z':
        case 'z':
            gchar_rotation = Z_AXIS_ROT;
            break;

			
		default:
			break;
			
	}
}

- (void)mouseDown:(NSEvent *)theEvent
{
	//code
}


- (void)mouseDragged:(NSEvent *)theEvent
{
	//code
}

- (void)rightMouseDown:(NSEvent *)theEvent
{
	//code
}

- (void) dealloc
{
	//code

	
	// destroy vao
    if (vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
    

    // destroy vbo
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }

        // destroy vbo
    if (vbo_normals)
    {
        glDeleteBuffers(1, &vbo_normals);
        vbo_normals = 0;
    }
    // destroy vbo
    if (vbo_texture)
    {
        glDeleteBuffers(1, &vbo_texture);
        vbo_texture = 0;
    }
    
    // destroy vbo
    if (vbo_index)
    {
        glDeleteBuffers(1, &vbo_index);
        vbo_index = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
	
	CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
	
	[super dealloc];
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
