i#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

// global variables
FILE *gpFile=NULL;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point func
int main(int argc,const char * argv[])
{

	//code
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];

	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
	
}

// interface implementation
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	
	//code
	// log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
	
	// code// window
	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);
	
	// create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect
									styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									backing:NSBackingStoreBuffered
									defer:NO];
	[window setTitle:@"macOS OGLPP"];
	[window center];
	
	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
	
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	// code
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}


- (void)dealloc
{
	//code
	[glView release];
	
	[window release];
	
	[super dealloc];
	
}
@end

@implementation GLView
{
	@private
    CVDisplayLinkRef displayLink;
	
	GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_vertex;
	GLuint vbo_normals;
	
	GLfloat angleCube;

    GLuint mvpUniform;
    
    // for lights
    GLuint modelViewMatrixUniform, projectionMatrixUniform;
    GLuint ldUniform, kdUniform, lightPositionUniform;
    
    bool isLKeyPressed, isAKeyPressed;
    bool animate, light;
    
    GLuint lKeyPressedUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(NSRect)frame; // confirm the semicolon
{

	//code
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            // Sir gave us 4_1 core profile code but on Manisha's MacBook 3_2 profile support was available
            // so here onwards core profile has been changed to 3_2 and shader version to 330
            // It does not affect much to the shaders since we are not using any built in function from latest shader versions
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion3_2Core,
            // Specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
		}; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available. Exitting ...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version  : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version    : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   // *** VERTEX SHADER ***
    // create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
    "#version 330" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_LKeyPressed;" \
    "uniform vec3 u_Ld;" \
    "uniform vec3 u_Kd;" \
    "uniform vec4 u_light_position;" \
    "out vec3 diffuse_light;" \
    "void main(void)" \
    "{" \
    "if (u_LKeyPressed == 1)" \
    "{" \
    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
    "vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);" \
    "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" \
    "diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" \
    "}" \
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
    "}";
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // re-initialize
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
    "#version 330" \
    "\n" \
    "in vec3 diffuse_light;" \
    "out vec4 FragColor;" \
    "uniform int u_LKeyPressed;" \
    "void main(void)" \
    "{" \
    "vec4 color;" \
    "if (u_LKeyPressed == 1)" \
    "{" \
    "color = vec4(diffuse_light,1.0);" \
    "}" \
    "else" \
    "{" \
    "color = vec4(1.0, 1.0, 1.0, 1.0);" \
    "}" \
    "FragColor = color;" \
    "}";
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    shaderProgramObject = glCreateProgram();
    
    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    // attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // get MVP uniform location
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    
    // get uniform locations
    modelViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_view_matrix");
    projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
    
    lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_LKeyPressed");
    
    ldUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
    kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");;
    
	// Pyramid
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    /* const GLfloat pyramidVertices[] =
    {
        0.0f, 1.0f,  0.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        0.0f, 1.0f,  0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        0.0f, 1.0f,  0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f,-1.0f, -1.0f,
        0.0f, 1.0f,  0.0f,
        -1.0f,-1.0f, -1.0f,
        -1.0f,-1.0f, 1.0f
    };
    
	const GLfloat pyramidColors[] =
    {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f
    };
	
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);
    
    glGenBuffers(1, &vbo_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	 
    glGenBuffers(1, &vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidColors), pyramidColors, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	
    glBindVertexArray(0);*/
	
	// Cube
	const GLfloat cubeVertices[] =
    {
        0.75f, 0.75f, -0.75f, // top
        0.75f, 0.75f, 0.75f,
        -0.75f, 0.75f, 0.75f,
        -0.75f, 0.75f, -0.75f,
        
        0.75f, -0.75f, -0.75f, // bottom
        0.75f, -0.75f, 0.75f,
        -0.75f, -0.75f, 0.75f,
        -0.75f, -0.75f, -0.75f,
        
        0.75f, 0.75f, 0.75f, // front
        0.75f, -0.75f, 0.75f,
        -0.75f, -0.75f, 0.75f,
        -0.75f, 0.75f, 0.75f,
        
        -0.75f, 0.75f, -0.75f, // back
        -0.75f, -0.75f, -0.75f,
        0.75f, -0.75f, -0.75f,
        0.75f, 0.75f, -0.75f,
        
        0.75f, 0.75f, -0.75f, // right
        0.75f, -0.75f, -0.75f,
        0.75f, -0.75f, 0.75f,
        0.75f, 0.75f, 0.75f,
        
        -0.75f, 0.75f, 0.75f, // left
        -0.75f, -0.75f, 0.75f,
        -0.75f, -0.75f, -0.75f,
        -0.75f, 0.75f, -0.75f
    };
	
	const GLfloat cubeNormals[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f
    };
    
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);
    
    glGenBuffers(1, &vbo_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glGenBuffers(1, &vbo_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
	
	
    
    glClearDepth(1.0f);
    // enable depth testing
    glEnable(GL_DEPTH_TEST);
    // depth test to do
    glDepthFunc(GL_LEQUAL);
    // We will always cull back faces for better performance
    //glEnable(GL_CULL_FACE);  ignored only for this assignment
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue
    
    // set projection matrix to identity matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
        height=1;
	
	
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
    //glOrtho(left,right,bottom,top,near,far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (width / height), 1.0f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}


- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];	
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    // OpenGL Drawing
    // set modelview & modelviewprojection matrices to identity
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
	// push triangle deeper-left for perspective
	/*modelViewMatrix = vmath::translate(-1.5f,0.0f,-6.0f);
	
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f,1.0f,0.0f);
	
	modelViewMatrix = modelViewMatrix * rotationMatrix;
	
    // multiply the modelview and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
	
	// pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
    // *** bind vao ***
    glBindVertexArray(vao_pyramid);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 12); // 3 (each with its x,y,z ) vertices in triangleVertices array
    
    // *** unbind vao ***
    glBindVertexArray(0);
	
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();*/
    
    if (light == true)
    {
        glUniform1i(lKeyPressedUniform, 1);
        
        glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
        glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);
        
        float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
        glUniform4fv(lightPositionUniform, 1, (GLfloat *)lightPosition);
    }
    else
    {
        glUniform1i(lKeyPressedUniform, 0);
    }
    
    
    // push square deeper-right for perspective
	modelViewMatrix = vmath::translate(0.0f,0.0f,-4.0f);
	
    rotationMatrix = vmath::rotate(angleCube, angleCube,angleCube);
	
	modelViewMatrix = modelViewMatrix * rotationMatrix;
	
	// multiply the modelview and orthographic matrix to get modelviewprojection matrix
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
	
    // pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
	    // *** bind vao ***
    glBindVertexArray(vao_cube);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4); // 3 (each with its x,y,z ) vertices in triangleVertices array
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    
    // *** unbind vao ***
    glBindVertexArray(0);
    

    
    // stop using OpenGL program object
    glUseProgram(0);

    if(animate == true)
    {
        [self update];
    }
	
	
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


- (void)update
{
	angleCube += 1.0f;
	if(angleCube >= 360.0f)
	{
		angleCube = 0.0f;
	}
/*
	anglePyramid += 0.5f;
	if(anglePyramid >= 360.0f)
	{
		anglePyramid = 0.0f;
	}*/
}

- (BOOL)acceptsFirstResponder
{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}

- (void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: //Esc
			[self release];
			[NSApp terminate:self];
			break;
		
		case 'F':
		case 'f':
			//centralText=@"'F' or 'f' Key Is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
            
        case 'A': // for 'A' or 'a'
        case 'a':
            if (isAKeyPressed == false)
            {
                animate = true;
                isAKeyPressed = true;
            }
            else
            {
                animate = false;
                isAKeyPressed = false;
            }
            break;
            
        case 'L': // for 'L' or 'l'
        case 'l':
            if (isLKeyPressed == false)
            {
                light = true;
                isLKeyPressed = true;
            }
            else
            {
                light = false;
                isLKeyPressed = false;
            }
            break;

			
		default:
			break;
			
	}
}

- (void)mouseDown:(NSEvent *)theEvent
{
	//code
}


- (void)mouseDragged:(NSEvent *)theEvent
{
	//code
}

- (void)rightMouseDown:(NSEvent *)theEvent
{
	//code
}

- (void) dealloc
{
	//code
	// destroy vao
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
	
	// destroy vao
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    

    // destroy vbo
    if (vbo_vertex)
    {
        glDeleteBuffers(1, &vbo_vertex);
        vbo_vertex = 0;
    }

        // destroy vbo
    if (vbo_normals)
    {
        glDeleteBuffers(1, &vbo_normals);
        vbo_normals = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
	
	CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
	
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
