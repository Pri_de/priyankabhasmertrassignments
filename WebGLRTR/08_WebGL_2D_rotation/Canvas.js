// global variables
var canvas = null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var gl = null;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vao_square;
var vbo_vertex;
var vbo_color;

var mvpUniform;

var angleTri =0.0;
var angleSqr =0.0;

var perspectiveProjectionMatrix;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


//onload function
function main()
{
		//get <Canvas> element
		canvas = document.getElementById("AMC");
		if(!canvas)
			console.log("Obtaining Canvas Failed\n");
		else
			console.log("obtaining Canvas Succeeded\n");
	    canvas_original_width=canvas.width;
	    canvas_original_height=canvas.height;

		// print canvas width and height
		console.log("Canvas width = " + canvas.width+" and Canvas Height = "+ canvas.height);
		
	    // register keyboard's keydown event handler
    	window.addEventListener("keydown", keyDown, false);
    	window.addEventListener("click", mouseDown, false);
    	window.addEventListener("resize", resize, false);
		
		// initialize WebGL
	    init();
	    
	    // start drawing here as warming-up
	    resize();
	    draw();
		
				
}


function toggleFullScreen()
{
	
	console.log("inside full screen");
	// code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	
	// if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullscreen)
			canvas.mozRequestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscree=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullscreen)
			document.mozCancelFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();		
		bFullscreen=false;
	}
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
	console.log("got rendering context for WebGL");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 color_out;"+
    "uniform mat4 u_mvp_matrix;"+
    "void main(void)"+
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
	"color_out = vColor;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
	"precision highp float;"+
	"in vec4 color_out;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = color_out;"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");
    
    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location
    mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
    
	//TRIANGLE
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    var triangleVertices=new Float32Array([
                                           0.0,  1.0, 0.0,   // appex
                                           -1.0, -1.0, 0.0, // left-bottom
                                           1.0, -1.0, 0.0   // right-bottom
                                           ]);

    vao_triangle=gl.createVertexArray();
    gl.bindVertexArray(vao_triangle);
    
    vbo_vertex = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_vertex);
    gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	
	var triangleColors=new Float32Array([
                                           1.0,  0.0, 0.0,   // appex
                                           0.0,  1.0, 0.0, // left-bottom
                                           0.0,  0.0, 1.0   // right-bottom
                                           ]);
	vbo_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, triangleColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,
                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
                           gl.FLOAT,
                           false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);									   
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
										   
    gl.bindVertexArray(null);

	
	//SQUARE
	// *** vertices, colors, shader attribs, vbo, vao initializations ***
    var squareVertices=new Float32Array([
                                           1.0,  1.0, 0.0,   // right-top
										   -1.0,  1.0, 0.0,   // left-top
                                           -1.0, -1.0, 0.0, // left-bottom
                                           1.0, -1.0, 0.0   // right-bottom
                                           ]);

    vao_square=gl.createVertexArray();
    gl.bindVertexArray(vao_square);
    
    vbo_vertex = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_vertex);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	
	var squareColors=new Float32Array([
										   0.0,  0.0, 1.0,
                                           0.0,  0.0, 1.0,   
                                           0.0,  0.0, 1.0, 
                                           0.0,  0.0, 1.0   
                                           ]);
	vbo_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, squareColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,
                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
                           gl.FLOAT,
                           false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);									   
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
										   
    gl.bindVertexArray(null);

	
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match1
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection => left,right,bottom,top,near,far
   // if (canvas.width <= canvas.height)
   mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
   // else
   //     mat4.perspective(perspectiveProjectionMatrix, ((parse float)-100.0 * (parse float)(canvas.width / canvas.height)), (parse float)(100.0 * (canvas.width / canvas.height)), (parse float)-100.0, (parse float)100.0, (parse float)-100.0, (parse float)100.0);
}


function keyDown(event)
{
	// code
	//alert("A key is pressed");
	
	switch(event.keyCode)
	{


        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;

		case 70: // for 'f' anf 'F'
			toggleFullScreen();
			break;
	}
}




function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix=mat4.create();
    var modelViewProjectionMatrix=mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5,0.0,-6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleTri));
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_triangle);

    gl.drawArrays(gl.TRIANGLES,0,3);
    
    gl.bindVertexArray(null);
	
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5,0.0,-6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleSqr));
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(vao_square);

    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    
    gl.bindVertexArray(null);
    
    gl.useProgram(null);
    
    // animation loop
    requestAnimationFrame(draw, canvas);
	update();
}


function update()
{
	angleTri+= 0.5;
	if(angleTri >= 360.0)
	{
		angleTri= 0.0;
		
	}
	
	angleSqr+= 1.0;
	if(angleSqr>= 360.0)
	{
		angleSqr = 0.0;
	}
}

function uninitialize()
{
    // code
    if(vao_triangle)
    {
        gl.deleteVertexArray(vao);
        vao=null;
    }
    
    if(vbo_vertex)
    {
        gl.deleteBuffer(vbo);
        vbo=null;
    }
	
	if(vbo_color)
    {
        gl.deleteBuffer(vbo_color);
        vbo=null;
    }
    
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}