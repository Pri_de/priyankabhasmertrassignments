// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_position;
var vbo_texture;
var mvpUniform;

var perspectiveProjectionMatrix;

var pyramid_texture=0;
var uniform_texture0_sampler;

var anglePyramid=0.0;
var smiley_texture=0;
var gCheckerBoardTexture=0;
var angleCube=0.0;

// for proceedural texture
var checkImageWidth = 64;
var checkImageHeight = 64;

var checkImage = new Uint8Array(checkImageWidth*checkImageHeight*4);
var gKeyPressed = 5;
var quadTexcoords;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec2 vTexture0_Coord;"+
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;"+
    "void main(void)"+
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
    "out_texture0_coord = vTexture0_Coord;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec2 out_texture0_coord;"+
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

   
    // Load cube Textures
    smiley_texture = gl.createTexture();
    smiley_texture.image = new Image();
    smiley_texture.image.src="Smiley.bmp";
    smiley_texture.image.onload = function ()
    {
        gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, smiley_texture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }
    
    // get MVP uniform location
    mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
    // get texture0_sampler uniform location
    uniform_texture0_sampler=gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

    
    var quadVertices=new Float32Array([
                                      /* // top surface
                                       1.0, 1.0,-1.0,  // top-right of top
                                       -1.0, 1.0,-1.0, // top-left of top
                                       -1.0, 1.0, 1.0, // bottom-left of top
                                       1.0, 1.0, 1.0,  // bottom-right of top
                                       
                                       // bottom surface
                                       1.0,-1.0, 1.0,  // top-right of bottom
                                       -1.0,-1.0, 1.0, // top-left of bottom
                                       -1.0,-1.0,-1.0, // bottom-left of bottom
                                       1.0,-1.0,-1.0,  // bottom-right of bottom*/
                                       
                                       // front surface
                                       1.0, 1.0, 1.0,  // top-right of front
                                       -1.0, 1.0, 1.0, // top-left of front
                                       -1.0,-1.0, 1.0, // bottom-left of front
                                       1.0,-1.0, 1.0,  // bottom-right of front
                                       
                                    /*   // back surface
                                       1.0,-1.0,-1.0,  // top-right of back
                                       -1.0,-1.0,-1.0, // top-left of back
                                       -1.0, 1.0,-1.0, // bottom-left of back
                                       1.0, 1.0,-1.0,  // bottom-right of back
                                       
                                       // left surface
                                       -1.0, 1.0, 1.0, // top-right of left
                                       -1.0, 1.0,-1.0, // top-left of left
                                       -1.0,-1.0,-1.0, // bottom-left of left
                                       -1.0,-1.0, 1.0, // bottom-right of left
                                       
                                       // right surface
                                       1.0, 1.0,-1.0,  // top-right of right
                                       1.0, 1.0, 1.0,  // top-left of right
                                       1.0,-1.0, 1.0,  // bottom-left of right
                                       1.0,-1.0,-1.0,  // bottom-right of right*/
                                       ]);
    
    // If above -1.0 Or +1.0 Values Make Cube Much Larger Than Pyramid,
    // then follow the code in following loop which will convertt all 1s And -1s to -0.75 or +0.75
    /*for(var i=0;i<72;i++)
    {
        if(quadVertices[i]<0.0)
            quadVertices[i]=quadVertices[i]+0.25;
        else if(quadVertices[i]>0.0)
            quadVertices[i]=quadVertices[i]-0.25;
        else
            quadVertices[i]=quadVertices[i]; // no change
    }*/
    
    quadTexcoords=new Float32Array([
                                        /*0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,*/
                                        
										1.0,1.0,
										0.0,1.0,
										0.0,0.0,
										1.0,0.0
                                        
                                        /*0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,
                                        
                                        0.0,0.0,
                                        1.0,0.0,
                                        1.0,1.0,
                                        0.0,1.0,*/
                                        ]);

    

    // smiley on quad code
    vao=gl.createVertexArray();
    gl.bindVertexArray(vao);
    
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER,quadVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, // 3 is for X,Y,Z co-ordinates in our triangleVertices array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    vbo_texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
    gl.bufferData(gl.ARRAY_BUFFER,quadTexcoords,gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, // 2 is for S and T co-ordinates in our pyramidTexcoords array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);
    
	
	loadProceeduralTexture();
	
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // black
    
    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function copy(obj) {
  const copy = Object.create(Object.getPrototypeOf(obj));
  const propNames = Object.getOwnPropertyNames(obj);

  propNames.forEach(function(name) {
    const desc = Object.getOwnPropertyDescriptor(obj, name);
    Object.defineProperty(copy, name, desc);
  });
}



function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix=mat4.create(); // itself creates identity matrix
    var modelViewProjectionMatrix=mat4.create(); // itself creates identity matrix

    

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,0.0,-3.0]);
    
    mat4.rotateX(modelViewMatrix ,modelViewMatrix, degToRad(angleCube));
    mat4.rotateY(modelViewMatrix ,modelViewMatrix, degToRad(angleCube));
    mat4.rotateZ(modelViewMatrix ,modelViewMatrix, degToRad(angleCube));
    
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
    
    // bind with texture
	
	switch (gKeyPressed)
	{

		case 1:
			// roght quarter of the smiley
			/*var quarterSmiley= new Float32Array([0.5,0.5,
										0.0,0.5,
										0.0,0.0,
										0.5,0.0
                                        ]);*/
			quadTexcoords[0] = 0.5;
			quadTexcoords[1] = 0.5;
			quadTexcoords[2] = 0.0;
			quadTexcoords[3] = 0.5;
			quadTexcoords[4] = 0.0;
			quadTexcoords[5] = 0.0;
			quadTexcoords[6] = 0.5;
			quadTexcoords[7] = 0.0;
			//quadTexcoords=copy(quarterSmiley);
			gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
			break;

		case 2:
			// normal smiley
			quadTexcoords[0] = 1.0;
			quadTexcoords[1] = 1.0;
			quadTexcoords[2] = 0.0;
			quadTexcoords[3] = 1.0;
			quadTexcoords[4] = 0.0;
			quadTexcoords[5] = 0.0;
			quadTexcoords[6] = 1.0;
			quadTexcoords[7] = 0.0;
			gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
			break;

		case 3:
			// four smileies
			quadTexcoords[0] = 2.0;
			quadTexcoords[1] = 2.0;
			quadTexcoords[2] = 0.0;
			quadTexcoords[3] = 2.0;
			quadTexcoords[4] = 0.0;
			quadTexcoords[5] = 0.0;
			quadTexcoords[6] = 2.0;
			quadTexcoords[7] = 0.0;
			gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
			break;


		case 4:
			// center yellow dot from smiley
			quadTexcoords[0] = 0.5;
			quadTexcoords[1] = 0.5;
			quadTexcoords[2] = 0.5;
			quadTexcoords[3] = 0.5;
			quadTexcoords[4] = 0.5;
			quadTexcoords[5] = 0.5;
			quadTexcoords[6] = 0.5;
			quadTexcoords[7] = 0.5;
			gl.bindTexture(gl.TEXTURE_2D, smiley_texture);
			break;

		case 5:
			// normal white procedural texture
			quadTexcoords[0] = 1.0;
			quadTexcoords[1] = 1.0;
			quadTexcoords[2] = 0.0;
			quadTexcoords[3] = 1.0;
			quadTexcoords[4] = 0.0;
			quadTexcoords[5] = 0.0;
			quadTexcoords[6] = 1.0;
			quadTexcoords[7] = 0.0;
			gl.bindTexture(gl.TEXTURE_2D, gCheckerBoardTexture);
			break;
			
		default:
			break;
	}
    //gl.bindTexture(gl.TEXTURE_2D,gCheckerBoardTexture);
    gl.uniform1i(uniform_texture0_sampler, 0);
    
    gl.bindVertexArray(vao);
	
	//to send texcord data dynamically
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER,quadTexcoords,gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    // actually 2 triangles make 1 cube, so there should be 6 vertices,
    // but as 2 tringles while making square meet each other at diagonal,
    // 2 of 6 vertices are common to both triangles, and hence 6-2=4
    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    /*gl.drawArrays(gl.TRIANGLE_FAN,4,4);
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
    gl.drawArrays(gl.TRIANGLE_FAN,12,4);
    gl.drawArrays(gl.TRIANGLE_FAN,16,4);
    gl.drawArrays(gl.TRIANGLE_FAN,20,4);*/
    
    gl.bindVertexArray(null);

    gl.useProgram(null);
    
   
    /*angleCube=angleCube+2.0;
    if(angleCube>=360.0)
        angleCube=angleCube-360.0;*/
    
    // animation loop
    requestAnimationFrame(draw, canvas);
}



function MakeCheckImage()
{
	
			//code
		var i, j, k, c, arrayIndex=0;

		for (i = 0; i < checkImageHeight; i++) {

			for (j = 0; j < checkImageWidth; j++) {

				if((((i & 0x8) == 0) ^ ((j & 0x8) == 0)))
					c = 255;
				else
					c = 0;
				
				// IMP : you need 1D image array for ByteBuffer class. Unlike 3D arrays for Windows and Linux
				for(k = 0; k<4; k++) {
					
					
					if(k == 0)
						checkImage[arrayIndex] = 255;
						//checkImage[i][j][k] = (byte)255;
					else if(k ==1)
						checkImage[arrayIndex] = 255;
						//checkImage[i][j][k] = (byte)c;
					else if(k ==2)
						checkImage[arrayIndex] = 255;
						//checkImage[i][j][k] = (byte)c;
					else
						checkImage[arrayIndex] = 255;//(byte)255;
						//checkImage[i][j][k] = (byte)255;
				
					arrayIndex++;
				}
				
				
			}
		}
	
	
}
function loadProceeduralTexture()
{
	
	MakeCheckImage();
	gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1);
	gCheckerBoardTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, gCheckerBoardTexture);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	//generate mipmapped texture(3bytes, width, height and datafrom bmp)
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, checkImageWidth, checkImageHeight, 0,
		gl.RGBA, gl.UNSIGNED_BYTE, checkImage);

	//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}





function uninitialize()
{

    
    if(smiley_texture)
    {
        gl.deleteTexture(smiley_texture);
        smiley_texture=0;
    }
    
	if(gCheckerBoardTexture)
    {
        gl.deleteTexture(gCheckerBoardTexture);
        gCheckerBoardTexture=0;
    }

    if(vao)
    {
        gl.deleteVertexArray(vao);
        vao=null;
    }

    if(vbo_texture)
    {
        gl.deleteBuffer(vbo_texture);
        vbo_texture=null;
    }
    
    if(vbo_position)
    {
        gl.deleteBuffer(vbo_position);
        vbo_position=null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
			
			
		case 0x31:
		case 0x61:
			gKeyPressed = 1;
			//alert("1 key is pressed");
			break;

		case 0x32:
		case 0x62:
			gKeyPressed = 2;
			break;

		case 0x33:
		case 0x63:
			gKeyPressed = 3;
			break;

		case 0x34:
		case 0x64:
			gKeyPressed = 4;
			break;

		case 0x35:
		case 0x65:
			gKeyPressed = 5;
			break;
			
			
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}
