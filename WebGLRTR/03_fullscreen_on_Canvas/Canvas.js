// global variables
var canvas = null;
var context = null;


//onload function
function main()
{
		//get <Canvas> element
		canvas = document.getElementById("AMC");
		if(!canvas)
			console.log("Obtaining Canvas Failed\n");
		else
			console.log("obtaining Canvas Succeeded\n");
		
		// print canvas width and height
		console.log("Canvas width = " + canvas.width+" and Canvas Height = "+ canvas.height);
		
		// get 2d Context
		context = canvas.getContext("2d");
		if(!context)
			console.log("Obtaining 2d context Failed\n");
		else
			console.log("obtaining 2d context Succeeded\n");
	
		
		// draw text
		drawText("Hello World..!!!");
		
		
		// register keyboard's keydown event handler
		window.addEventListener("keydown", keydown, false);
		window.addEventListener("click", mousedown, false);
				
}

function drawText(text)
{
	// code
	
	// fill canvas with black color
	context.fillStyle = "black" //"#000000"
	context.fillRect(0,0,canvas.width, canvas.height);
	
	
	
	//center text
	context.textAlign ="center"; // horizontally at center
	context.textBaseline = "middle";// vertically  at center
	
	//font
	context.font="48px sans-serif";
		
	//text color
	context.fillStyle="white" // "#FFFFFF"
		
	//display the text in center
	context.fillText(text,canvas.width/2, canvas.height/2);
	
}

function toggleFullScreen()
{
	// code
	var fullscreen_element = document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	
	// if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullscreen)
			canvas.mozRequestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullscreen)
			document.mozCancelFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();		
		
	}
}



function keydown(event)
{
	// code
	//alert("A key is pressed");
	
	switch(event.keyCode)
	{
		case 70: // for 'f' anf 'F'
			toggleFullScreen();
			
			//repaint
			drawText("Hello world");
			break;
	}
}


function mousedown(event)
{
	// code
	alert("Mouse is clicked");
}