// global variables
var canvas = null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var gl = null;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//for light
var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

var gstMaterialProperties =
[
// ***** 1st sphere on 1st column, emerald *****
	[0.0215,0.1745,0.0215],//Ambient
	[0.0768,0.61424,0.07568],//Diffused
	[0.633,0.727811,0.633],//Specular
	[0.6 * 128],//Shininess

	// ***** 2nd sphere on 1st column, jade *****
	[0.135 ,0.2225 ,0.1575],//Ambient
	[0.54,0.89,0.63],//Diffused
	[0.316228,0.316228,0.316228],//Specular
	[0.1 * 128],//Shininess

	// ***** 3rd sphere on 1st column obsidian *****
	[0.05375 ,0.05 ,0.06625],//Ambient
	[0.18275,0.17 ,0.22525],//Diffused
	[0.332741,0.328634,0.346435],//Specular
	[0.3 * 128],//Shininess

	// ***** 4th sphere on 1st column pearl *****
	[0.25 ,0.20725 ,0.20725],//Ambient
	[1.0  ,0.829,0.829],//Diffused
	[0.296648,0.296648,0.296648],//Specular
	[0.088 * 128],//Shininess

	// ***** 5th sphere on 1st column ruby *****
	[0.1745 ,0.01175,0.01175],//Ambient
	[0.61424,0.04136,0.04136],//Diffused
	[0.727811,0.626959,0.626959],//Specular
	[0.6 * 128],//Shininess

	// ***** 6th sphere on 1st column turquoise *****
	[0.1 ,0.18725,0.1745],//Ambient
	[0.396,0.74151,0.69102],//Diffused
	[0.297254,0.30829,0.306678],//Specular
	[0.1 * 128],//Shininess

	// ***** 1st sphere on 2nd column brass *****
	[0.329412 ,0.223529,0.027451],//Ambient
	[0.780392,0.568627,0.113725],//Diffused
	[0.992157,0.941176,0.807843],//Specular
	[0.21794872 * 128],//Shininess

	// ***** 2nd sphere on 2nd column bronze *****
	[0.2125 ,0.1275 ,0.054],//Ambient
	[0.714,0.4284 ,0.18144],//Diffused
	[0.393548,0.271906,0.166721],//Specular
	[0.2 * 128],//Shininess

	// ***** 3rd sphere on 2nd column chrome *****
	[0.25,0.25,0.25],//Ambient
	[0.4,0.4,0.4],//Diffused
	[0.774597,0.774597,0.774597],//Specular
	[0.6 * 128],//Shininess

	// ***** 4th sphere on 2nd column copper *****
	[0.19125 ,0.0735 ,0.0225],//Ambient
	[0.7038,0.27048 ,0.0828],//Diffused
	[0.256777,0.137622,0.086014],//Specular
	[0.1 * 128],//Shininess

	// ***** 5th sphere on 2nd column gold *****
	[0.24725,0.1995 ,0.0745],//Ambient
	[0.75164,0.60648,0.22648],//Diffused
	[0.628281,0.555802,0.366065],//Specular
	[0.4 * 128],//Shininess

	// ***** 6th sphere on 2nd column silver *****
	[0.19225 ,0.19225 ,0.19225],//Ambient
	[0.50754,0.50754,0.50754],//Diffused
	[0.508273,0.508273,0.508273],//Specular
	[0.4 * 128],//Shininess

	// ***** 1st sphere on 3rd column black *****
	[0.0,0.0,0.0],//Ambient
	[0.0,0.0,0.0],//Diffused
	[0.50 ,0.50 ,0.50],//Specular
	[0.25 * 128],//Shininess

	// ***** 2nd sphere on 3rd column cyan *****
	[0.0 ,0.1,0.06],//Ambient
	[0.0 ,0.50980392,0.50980392],//Diffused
	[0.50196078,0.50196078,0.50196078],//Specular
	[0.25 * 128],//Shininess

	// ***** 3rd sphere on 2nd column green *****
	[0.0 ,0.0 ,0.0],//Ambient
	[0.1,0.35,0.1],//Diffused
	[0.45,0.55 ,0.45],//Specular
	[0.25 * 128],//Shininess

	// ***** 4th sphere on 3rd column red *****
	[0.0,0.0,0.0],//Ambient
	[0.5,0.0,0.0],//Diffused
	[0.7,0.6 ,0.6],//Specular
	[0.25 * 128],//Shininess

	// ***** 5th sphere on 3rd column white *****
	[0.0,0.0,0.0],//Ambient
	[0.55 ,0.55 ,0.55],//Diffused
	[0.70, 0.70, 0.70],//Specular
	[0.25 * 128],//Shininess

	// ***** 6th sphere on 3rd column yellow plastic *****
	[0.0, 0.0, 0.0],//Ambient
	[0.5, 0.5, 0.0],//Diffused
	[0.60, 0.60, 0.50],//Specular
	[0.25 * 128],//Shininess

	// ***** 1st sphere on 4th column black *****
	[0.02, 0.02, 0.02],//Ambient
	[0.01, 0.01, 0.01],//Diffused
	[0.4, 0.4, 0.4],//Specular
	[0.078125 * 128],//Shininess

	// ***** 2nd sphere on 4th column cyan *****
	[0.0, 0.05, 0.05],//Ambient
	[0.4, 0.5, 0.5],//Diffused
	[0.04, 0.7, 0.7],//Specular
	[0.078125 * 128],//Shininess

	// ***** 3rd sphere on 4th column green *****
	[0.0, 0.05, 0.0], //Ambient
	[0.4, 0.5, 0.4], //Diffused
	[0.04, 0.7, 0.04], //Specular
	[0.078125 * 128],//Shininess

	// ***** 4th sphere on 4th column red *****
	[0.05, 0.0, 0.0], //Ambient
	[0.5, 0.4, 0.4], //Diffused
	[0.7, 0.04, 0.04], //Specular
	[0.078125 * 128],//Shininess

	// ***** 5th sphere on 4th column white *****
	[0.05, 0.05, 0.05], //Ambient
	[0.5, 0.5, 0.5], //Diffused
	[0.7, 0.7, 0.7], //Specular
	[0.078125 * 128],//Shininess

	// ***** 6th sphere on 4th column yellow rubber *****
	[0.05, 0.05, 0.0], //Ambient
	[0.5, 0.5, 0.4], //Diffused
	[0.7, 0.7, 0.04], //Specular
	[0.078125 * 128],//Shininess

];

var gchar_rotation = 0;
var X_AXIS_ROT = 0;
var Y_AXIS_ROT = 1;
var Z_AXIS_ROT = 2;
var myangle=0;

var sphere=null;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldUniform, kdUniform, lightPositionUniform;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var LKeyPressedUniform;

var bLKeyPressed=false;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


//onload function
function main()
{
		//get <Canvas> element
		canvas = document.getElementById("AMC");
		if(!canvas)
			console.log("Obtaining Canvas Failed\n");
		else
			console.log("obtaining Canvas Succeeded\n");
	    canvas_original_width=canvas.width;
	    canvas_original_height=canvas.height;

		// print canvas width and height
		console.log("Canvas width = " + canvas.width+" and Canvas Height = "+ canvas.height);
		
	    // register keyboard's keydown event handler
    	window.addEventListener("keydown", keyDown, false);
    	window.addEventListener("click", mouseDown, false);
    	window.addEventListener("resize", resize, false);
		
		// initialize WebGL
	    init();
	    
	    // start drawing here as warming-up
	    resize();
	    draw();
		
				
}


function toggleFullScreen()
{
	
	console.log("inside full screen");
	// code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	
	// if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscree=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();		
		bFullscreen=false;
	}
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
	console.log("got rendering context for WebGL");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
    "out vec3 light_direction;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
    "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
    "in vec3 light_direction;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
    "vec3 normalized_light_direction=normalize(light_direction);"+
    "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
    "vec3 ambient = u_La * u_Ka;"+
    "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
    "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
    "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
    "phong_ads_color=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "FragColor = vec4(phong_ads_color,1.0);"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
    
    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    
   // get Model Matrix uniform location
    modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
    
    // ambient color intensity of light
    laUniform=gl.getUniformLocation(shaderProgramObject,"u_La");
    // diffuse color intensity of light
    ldUniform=gl.getUniformLocation(shaderProgramObject,"u_Ld");
    // specular color intensity of light
    lsUniform=gl.getUniformLocation(shaderProgramObject,"u_Ls");
    // position of light
    lightPositionUniform=gl.getUniformLocation(shaderProgramObject,"u_light_position");
    
    // ambient reflective color intensity of material
    kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
 
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
	// Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match1
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection => left,right,bottom,top,near,far
   // if (canvas.width <= canvas.height)
   mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
   // else
   //     mat4.perspective(perspectiveProjectionMatrix, ((parse float)-100.0 * (parse float)(canvas.width / canvas.height)), (parse float)(100.0 * (canvas.width / canvas.height)), (parse float)-100.0, (parse float)100.0, (parse float)-100.0, (parse float)100.0);
}


function keyDown(event)
{
	// code
	//alert("A key is pressed");
	
	switch(event.keyCode)
	{


        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
			
			
		 case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
        break;
		
		case 88:
			gchar_rotation = X_AXIS_ROT;
			break;

		case 89:
			gchar_rotation = Y_AXIS_ROT;
			break;

		case 90:
			gchar_rotation = Z_AXIS_ROT;
			break;

		case 70: // for 'f' anf 'F'
			toggleFullScreen();
			break;
	}
}




function draw()
{
    // code
	var xPos = -5.0;
	var yPos = 3.0;
	var sphereCounter = 0;
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
	
	if (bLKeyPressed == true)
	{
		if (gchar_rotation == X_AXIS_ROT)
		{
			light_position[0] = 0.0;
			light_position[1] = 100 * Math.sin(myangle*Math.PI / 180);
			light_position[2] = 100 * Math.cos(myangle*Math.PI / 180);
		}
		else if (gchar_rotation == Y_AXIS_ROT)
		{
			light_position[1] = 0.0;
			light_position[0] = 100 * Math.sin(myangle*Math.PI / 180);
			light_position[2] = 100 * Math.cos(myangle*Math.PI / 180);
		}
		else if (gchar_rotation == Z_AXIS_ROT)
		{
			light_position[2] = 0.0;
			light_position[0] = 100 * Math.sin(myangle*Math.PI / 180);
			light_position[1] = 100 * Math.cos(myangle*Math.PI / 180);
		}
		// set 'u_lighting_enabled' uniform
		gl.uniform1i(LKeyPressedUniform, 1);

		// setting light's properties
		gl.uniform3fv(laUniform, light_ambient);
		gl.uniform3fv(ldUniform, light_diffuse);
		gl.uniform3fv(lsUniform, light_specular);
		gl.uniform4fv(lightPositionUniform, light_position);

		//// setting material's properties
		//gl.uniform3fv(Ka_uniform, 1, material_ambient);
		//gl.uniform3fv(Kd_uniform, 1, material_diffuse);
		//gl.uniform3fv(Ks_uniform, 1, material_specular);
		//gl.uniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set 'u_lighting_enabled' uniform
		gl.uniform1i(LKeyPressedUniform, 0);
	}
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();
    
	
    //mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-2.0]);
    //mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
	//viewMatrix = lookat(vec3(0.0, 0.0, 0.1), vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	for (var column = 0; column < 6; column++) {

		for (var row = 0; row < 4; row++) {

			//pushMatrix(&modelMatrix);
			mat4.translate(modelMatrix, modelMatrix, [xPos, yPos, -8.0]);
			//mytranslate(xPos, yPos, -10.0);
			gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
			gl.uniform3fv(kaUniform, gstMaterialProperties[sphereCounter*4]);
			gl.uniform3fv(kdUniform, gstMaterialProperties[sphereCounter*4+1]);
			gl.uniform3fv(ksUniform, gstMaterialProperties[sphereCounter*4+2]);
			gl.uniform1fv(materialShininessUniform, gstMaterialProperties[sphereCounter*4+3]);
			console.log("Inside draw()\n");
			//draw sphere
			makeSphere(sphere,0.6,30,30);
			sphere.draw();
			
			//popMatrix(&modelMatrix);
			yPos -= 2.0;
			
			// instead of push pop you load identity everytime
			modelMatrix=mat4.create();
			viewMatrix=mat4.create();
			sphereCounter++;
		}
		xPos += 2.0;
		yPos = 3.0;
	}
	
	
   
    gl.useProgram(null);
    // animation loop
    requestAnimationFrame(draw, canvas);
	update();
}


function update()
{
//
   myangle += 10;
}

function uninitialize()
{

	
    // code
    if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
	
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}