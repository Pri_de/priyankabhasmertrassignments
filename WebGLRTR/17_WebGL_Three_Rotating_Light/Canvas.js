// global variables
var canvas = null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;
var gl = null;
var PI = 3.142;
const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var material_ambient= [0.0,0.0,0.0];
var material_diffuse= [1.0,1.0,1.0];
var material_specular= [1.0,1.0,1.0];
var material_shininess= 50.0;

//Light0
var red_light0_ambient= [0.0, 0.0, 0.0];
var red_light0_diffuse= [1.0, 0.0, 0.0];
var red_light0_specular= [1.0, 0.0, 0.0];
var red_light0_position= [0.0, 0.0, -2.0, 0.0];

//Light1
var green_light1_ambient= [0.0, 0.0, 0.0];
var green_light1_diffuse= [0.0, 1.0, 0.0];
var green_light1_specular= [0.0, 1.0, 0.0];
var green_light1_position= [0.0, 0.0, -2.0, 0.0];

//Light2
var blue_light2_ambient= [0.0, 0.0, 0.0];
var blue_light2_diffuse= [0.0, 0.0, 1.0];
var blue_light2_specular= [0.0, 0.0, 1.0];
var blue_light2_position= [0.0, 0.0, -2.0, 0.0];

var La_uniform_red;
var Ld_uniform_red;
var Ls_uniform_red;
var light_position_uniform_red;

var La_uniform_green;
var Ld_uniform_green;
var Ls_uniform_green;
var light_position_uniform_green;

var La_uniform_blue;
var Ld_uniform_blue;
var Ls_uniform_blue;
var light_position_uniform_blue;

var angleLight = 0; // 3 * PI / 2;

var sphere=null;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var LKeyPressedUniform;

var bLKeyPressed=false;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


//onload function
function main()
{
		//get <Canvas> element
		canvas = document.getElementById("AMC");
		if(!canvas)
			console.log("Obtaining Canvas Failed\n");
		else
			console.log("obtaining Canvas Succeeded\n");
	    canvas_original_width=canvas.width;
	    canvas_original_height=canvas.height;

		// print canvas width and height
		console.log("Canvas width = " + canvas.width+" and Canvas Height = "+ canvas.height);
		
	    // register keyboard's keydown event handler
    	window.addEventListener("keydown", keyDown, false);
    	window.addEventListener("click", mouseDown, false);
    	window.addEventListener("resize", resize, false);
		
		// initialize WebGL
	    init();
	    
	    // start drawing here as warming-up
	    resize();
	    draw();
		
				
}


function toggleFullScreen()
{
	
	console.log("inside full screen");
	// code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	
	// if not fullscreen
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscree=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();		
		bFullscreen=false;
	}
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
	console.log("got rendering context for WebGL");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
    "uniform mat4 u_view_matrix;"+
    "uniform mat4 u_projection_matrix;"+
	"uniform vec4 u_light_position_red;"+
	"uniform vec4 u_light_position_green;"+
	"uniform vec4 u_light_position_blue;"+
    "uniform mediump int u_LKeyPressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normals;"+
	"out vec3 light_direction_red;"+
	"out vec3 light_direction_green;"+
	"out vec3 light_direction_blue;"+
    "out vec3 viewer_vector;"+
    "void main(void)"+
    "{"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
    "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
    "light_direction_red = vec3(u_light_position_red) - eye_coordinates.xyz;"+
	"light_direction_green = vec3(u_light_position_green) - eye_coordinates.xyz;"+
	"light_direction_blue = vec3(u_light_position_blue) - eye_coordinates.xyz;"+
    "viewer_vector = -eye_coordinates.xyz;"+
    "}"+
    "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec3 transformed_normals;"+
	"in vec3 light_direction_red;"+
	"in vec3 light_direction_green;"+
	"in vec3 light_direction_blue;"+
    "in vec3 viewer_vector;"+
    "out vec4 FragColor;"+
	"uniform vec3 u_La_red;"+
	"uniform vec3 u_Ld_red;"+
	"uniform vec3 u_Ls_red;"+
	"uniform vec3 u_La_green;"+
	"uniform vec3 u_Ld_green;"+
	"uniform vec3 u_Ls_green;"+
	"uniform vec3 u_La_blue;"+
	"uniform vec3 u_Ld_blue;"+
	"uniform vec3 u_Ls_blue;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_material_shininess;"+
    "uniform int u_LKeyPressed;"+
    "void main(void)"+
    "{"+
    "vec3 phong_ads_color;"+
    "if(u_LKeyPressed == 1)"+
    "{"+
    "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
    "vec3 normalized_light_direction=normalize(light_direction_red);"+
    "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
	"vec3 ambient = u_La_red * u_Ka;"+
	"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
	"vec3 diffuse = u_Ld_red * u_Kd * tn_dot_ld;"+
	"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
	"vec3 specular = u_Ls_red * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads_color=ambient + diffuse + specular;"+
		
	"normalized_transformed_normals=normalize(transformed_normals);"+
	"normalized_light_direction=normalize(light_direction_green);"+
	"normalized_viewer_vector=normalize(viewer_vector);"+
	"ambient = u_La_green * u_Ka;"+
	"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
	"diffuse = u_Ld_green * u_Kd * tn_dot_ld;"+
	"reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
	"specular = u_Ls_green * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads_color+=ambient + diffuse + specular;"+

	"normalized_transformed_normals=normalize(transformed_normals);"+
	"normalized_light_direction=normalize(light_direction_blue);"+
	"normalized_viewer_vector=normalize(viewer_vector);"+
	"ambient = u_La_blue * u_Ka;"+
	"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
	"diffuse = u_Ld_blue * u_Kd * tn_dot_ld;"+
	"reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
	"specular = u_Ls_blue * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads_color+=ambient + diffuse + specular;"+
    "}"+
    "else"+
    "{"+
    "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
    "}"+
    "FragColor = vec4(phong_ads_color,1.0);"+
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
    
    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    
   // get Model Matrix uniform location
    modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
    // get Projection Matrix uniform location
    projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    
    // get single tap detecting uniform
    LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
    
	// ambient color intensity of light red
	La_uniform_red = gl.getUniformLocation(shaderProgramObject, "u_La_red");
	// diffuse color intensity of light
	Ld_uniform_red = gl.getUniformLocation(shaderProgramObject, "u_Ld_red");
	// specular color intensity of light
	Ls_uniform_red = gl.getUniformLocation(shaderProgramObject, "u_Ls_red");
	// position of light
	light_position_uniform_red = gl.getUniformLocation(shaderProgramObject, "u_light_position_red");

	// ambient color intensity of light green
	La_uniform_green = gl.getUniformLocation(shaderProgramObject, "u_La_green");
	// diffuse color intensity of light
	Ld_uniform_green = gl.getUniformLocation(shaderProgramObject, "u_Ld_green");
	// specular color intensity of light
	Ls_uniform_green = gl.getUniformLocation(shaderProgramObject, "u_Ls_green");
	// position of light
	light_position_uniform_green = gl.getUniformLocation(shaderProgramObject, "u_light_position_green");

	// ambient color intensity of light blue
	La_uniform_blue = gl.getUniformLocation(shaderProgramObject, "u_La_blue");
	// diffuse color intensity of light
	Ld_uniform_blue = gl.getUniformLocation(shaderProgramObject, "u_Ld_blue");
	// specular color intensity of light
	Ls_uniform_blue = gl.getUniformLocation(shaderProgramObject, "u_Ls_blue");
	// position of light
	light_position_uniform_blue = gl.getUniformLocation(shaderProgramObject, "u_light_position_blue");
    
    // ambient reflective color intensity of material
    kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
    // diffuse reflective color intensity of material
    kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
    // specular reflective color intensity of material
    ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
    // shininess of material ( value is conventionally between 1 to 200 )
    materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
 
    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    sphere=new Mesh();
    makeSphere(sphere,2.0,30,30);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue
	// Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match1
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    // Orthographic Projection => left,right,bottom,top,near,far
   // if (canvas.width <= canvas.height)
   mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
   // else
   //     mat4.perspective(perspectiveProjectionMatrix, ((parse float)-100.0 * (parse float)(canvas.width / canvas.height)), (parse float)(100.0 * (canvas.width / canvas.height)), (parse float)-100.0, (parse float)100.0, (parse float)-100.0, (parse float)100.0);
}


function keyDown(event)
{
	// code
	//alert("A key is pressed");
	
	switch(event.keyCode)
	{


        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
			
			
		 case 76: // for 'L' or 'l'
            if(bLKeyPressed==false)
                bLKeyPressed=true;
            else
                bLKeyPressed=false;
        break;

		case 70: // for 'f' anf 'F'
			toggleFullScreen();
			break;
	}
}




function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed==true)
    {
        // rotate the light source in a circular fasion of perimeter of 4
		// across x- axis
		red_light0_position[1] = 10* Math.sin(angleLight);
		// -2 is to just bring the light aligned to the sphere, which is translated by -2 on z axis
		red_light0_position[2] = 10* Math.cos(angleLight) - 2.0;

		// across y- axis
		green_light1_position[0] = 10 * Math.sin(angleLight);
		green_light1_position[2] = 10* Math.cos(angleLight) - 2.0;
		// across z axis
		blue_light2_position[0] = 10 * Math.sin(angleLight);
		blue_light2_position[1] = 10 * Math.cos(angleLight);
		// set 'u_lighting_enabled' uniform
		gl.uniform1i(LKeyPressedUniform, 1);

		// setting light's properties red
		gl.uniform3fv(La_uniform_red, red_light0_ambient);
		gl.uniform3fv(Ld_uniform_red, red_light0_diffuse);
		gl.uniform3fv(Ls_uniform_red, red_light0_specular);
		gl.uniform4fv(light_position_uniform_red, red_light0_position);

		// setting light's properties green
		gl.uniform3fv(La_uniform_green, green_light1_ambient);
		gl.uniform3fv(Ld_uniform_green, green_light1_diffuse);
		gl.uniform3fv(Ls_uniform_green, green_light1_specular);
		gl.uniform4fv(light_position_uniform_green, green_light1_position);

		// setting light's properties blue
		gl.uniform3fv(La_uniform_blue, blue_light2_ambient);
		gl.uniform3fv(Ld_uniform_blue, blue_light2_diffuse);
		gl.uniform3fv(Ls_uniform_blue, blue_light2_specular);
		gl.uniform4fv(light_position_uniform_blue, blue_light2_position);
        
        // setting material properties
        gl.uniform3fv(kaUniform, material_ambient); // ambient reflectivity of material
        gl.uniform3fv(kdUniform, material_diffuse); // diffuse reflectivity of material
        gl.uniform3fv(ksUniform, material_specular); // specular reflectivity of material
        gl.uniform1f(materialShininessUniform, material_shininess); // material shininess
    }
    else
    {
        gl.uniform1i(LKeyPressedUniform, 0);
    }
    
    var modelMatrix=mat4.create();
    var viewMatrix=mat4.create();
    
	
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);
    
    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
    
    sphere.draw();
   
    gl.useProgram(null);
    // animation loop
    requestAnimationFrame(draw, canvas);
	update();
}


function update()
{
//
	angleLight += 0.03;
	if (angleLight >= 2 * PI) {
		angleLight = 0.0;
	}
}

function uninitialize()
{

	
    // code
    if(sphere)
    {
        sphere.deallocate();
        sphere=null;
    }
	
    
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}