//onload function
function main()
{
		//get <Canvas> element
		var canvas = document.getElementById("AMC");
		if(!canvas)
			console.log("Obtaining Canvas Failed\n");
		else
			console.log("obtaining Canvas Succeeded\n");
		
		// print canvas width and height
		console.log("Canvas width = " + canvas.width+" and Canvas Height = "+ canvas.height);
		
		// get 2d Context
		var context = canvas.getContext("2d");
		if(!context)
			console.log("Obtaining 2d context Failed\n");
		else
			console.log("obtaining 2d context Succeeded\n");
	
		// fill canvas with black color
		context.fillStyle = "black" //"#000000"
		context.fillRect(0,0,canvas.width, canvas.height);
		
		//center text
		context.textAlign ="center"; // horizontally at center
		context.textBaseline = "middle";// vertically  at center
		
		// text
		var str = "Hello world!!"
		//font
		context.font="48px sans-serif";
		
		//text color
		context.fillStyle="white" // "#FFFFFF"
		
		//display the text in center
		context.fillText(str,canvas.width/2, canvas.height/2);
				
		
}