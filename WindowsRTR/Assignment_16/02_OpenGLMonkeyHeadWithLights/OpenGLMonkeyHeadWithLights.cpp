// windows Header files
#include<Windows.h>

// OGL headers
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// c headers
#include <stdio.h>
#include <stdlib.h>

// c++ headers
#include <vector>

// Mesh loading ***************************
// symbolic constants

#define TRUE			1
#define FLASE			0

#define BUFFER_SIZE		256
#define S_EQUAL			0

#define WIN_INIT_X      100
#define WIN_INIT_Y      100
#define WIN_WIDTH       800 // extra
#define WIN_HEIGHT      600 // extra

#define VK_F			0x46
#define VK_f			0x60

#define NR_POINT_COORDS		3 // number of point coordinates
#define NR_TEXTURE_COORDS	2 // number of texture coords
#define NR_NORMAL_COORDS	3 // same for notrmal coords
#define NR_FACE_TOKENS		3 // minimum number of entries in face data

#define FOY_ANGLE			45
#define ZNEAR				0.1
#define ZFAR					200.0

#define MONKEYHEAD_X_TRANSLATE	0.0f
#define MONKEYHEAD_Y_TRANSLATE  -0.00f
#define MONKEYHEAD_Z_TRANSLSTE	-5.0f

#define MONKEYHEAD_X_SCALE_FACTOR	1.5f
#define MONKEYHEAD_Y_SCALE_FACTOR	1.5f
#define MONKEYHEAD_Z_SCALE_FACTOR	1.5f

#define START_ANGLE_POS				0.0f
#define END_ANGLE_POS				360.0f
#define MONKEYHEAD_ANGLE_INCREAMENT 1.0f

// import libraries
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Error handling macros
#define ERRORBOX1(lpszErrorMessage, lpszCaption) {														\
													MessageBox((HWND)NULL, TEXT(lpszErrorMessage)),		\
														TEXT(lpszCaption), MB_ICONERROR);				\
														ExitProcess(EXIT_FAILURE);						\
													}

#define ERRORBOX2(hwnd, lpszErrorMessage, lpszCaption) {												\
													MessageBox((HWND)NULL, TEXT(lpszErrorMessage)),		\
														TEXT(lpszCaption), MB_ICONERROR);				\
														DestroyWindow(EXIT_FAILURE);					\
													}



GLfloat g_rotate;

// vector of vector of floats to hold vertex data
std::vector<std::vector<float>> g_vertices;
// vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;
// vector of vector of floats to hold normal data
std::vector<std::vector<float>> g_normals;

// vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normal;

// handle to mesh file
FILE *g_fp_meshfile = NULL;

// handle to a log file
FILE *g_fp_logfile = NULL;

// hold line in from file
char line[BUFFER_SIZE];

// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898


float gwSideofTriangle = 2.0f;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone, gbLightingFlag = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

float anglePyramid, angleCube, angleSphere;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLUquadric *quadric = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);



//Lighting
GLboolean gbMonkey = GL_FALSE;

//Lighting
GLfloat light_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 5.0f, 0.0f, 1.0f, 0.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess[] = { 50.0f };

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("PBB"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{		
						
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
				update();
				display();

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}
       

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;*/
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case VK_f: //for 'f' or 'F'
		case VK_F:
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;

		case 'L':
			if (gbLightingFlag == false) {
				gbLightingFlag = true;
				glEnable(GL_LIGHTING);
			}
			else {
				gbLightingFlag = false;
				glDisable(GL_LIGHTING);
			}

			break;

		
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;

	/*case WM_CLOSE:
		uninitialize();
		break;*/

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);
	void uninitialize(void);
	void LoadMeshData(void); // mesh loader

	// code
	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;  // change for 3d effect


	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHT1);

	quadric = gluNewQuadric(); // for shpere
	
	// for 3D
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// could not get what difference it makes observed same thing. Ask Sir??
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // to avoid distortion due to perspective view
	glShadeModel(GL_SMOOTH);  // to avoid alaizing

	// read mesh file and load global vectors with appropriate data
	LoadMeshData();
	
	// warm up call to resize
	resize(WINWIDTH, WINHEIGHT);
}

void display(void) {

	void uninitialize(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // GL_DEPTH_BUFFER_BIT - For 3D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLSTE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);
	
	// keep counter-clockwise winding of vertices of geometry
	glFrontFace(GL_CCW);
	// set polygon mode mentioning front and back faces and gl_line
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);		
	
	for (int i = 0; i != g_face_tri.size(); ++i)
	{
		glBegin(GL_TRIANGLES);
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glNormal3f(g_normals[vi][0], g_normals[vi][1], g_normals[vi][2]);
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}

	SwapBuffers(ghdc);

}


void LoadMeshData(void)
{

	void uninitialize(void);

	// open meh file
	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	if (!g_fp_meshfile)
		uninitialize();

	// String space seperator for strtok
	char *sep_space = " ";

	// String forward slash separator for strtok
	char *sep_fslash = "/";

	// character pointer for holding first word in the line
	char *first_token = NULL;
	// character pointer for holding next word separated by strtok
	char *token = NULL;

	// Array of character pointers to hold face entries
	// Sometimes three, sometimes four
	char *face_tokens[NR_FACE_TOKENS];

	// number of non-null tokens in above vector
	int nr_tokens;

	// character pointer to hold string associated with
	// vector index
	char *token_vertex_index = NULL;
	// character pointer to hold string associated with
	// texture index 
	char *token_texture_index = NULL;
	// character pointer to hold string associated with
	// normal index
	char *token_normal_index = NULL;

	// while there is a line in the file
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		// Bind line to a separator and het first token
		first_token = strtok(line, sep_space);

		//if first token indicates vertex data
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			// create a vector of NR_POINT_CORDS number of floats
			// to hold coordinates
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			// loop to tokanize further details from the line
			for (int i = 0; i != NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			g_vertices.push_back(vec_point_coord);
		}

		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			g_texture.push_back(vec_texture_coord);
		}

		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normals.push_back(vec_normal_coord);
		}

		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			// INITIALISE all char pointer
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			// extract 3 fields of info in face token
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			// fetch traingle, texture and normal coordinate index data
			// from every face data entry 
			for (int i = 0; i != NR_FACE_TOKENS; ++i)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			// add constructed vectors to global face vectors
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normal.push_back(normal_vertex_indices);

		}

		// initialize line bufferto NULL
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}

	// close meshfile and make file pointer NULL
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	// log vertex, texture and face data in the log file
	fprintf(g_fp_logfile, "g_vertices:%llu g_texture: %llu g_normals: %llu g_face_tri: %llu\n",
		g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());

}


// update rotation angles
void update(void) {


	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREAMENT;
	if (g_rotate >= END_ANGLE_POS) {
		g_rotate = START_ANGLE_POS;
	}

}


void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	if (quadric != NULL) {
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	DestroyWindow(ghwnd);
}