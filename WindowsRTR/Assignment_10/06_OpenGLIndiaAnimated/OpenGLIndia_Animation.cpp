/* Priyanka Bhasme
* Native assignment # 06
* INDIA Animated
* Output - INDIA
*/

// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// Pragma
#pragma comment(lib, "opengl32.lib")
// we need this to link
#pragma comment(lib, "glu32.lib")


// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898


float anglePyramid, angleRect, angleDetla;
int rotationsCount = 10000;
float gwSideofTriangle = 2.0f;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
// ask sir - how error goes when curly brackets are added????????????
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

float xMoveI = -2.5f;
float yMoveN = 3.0f;
float yMoveI = -3.0f;
float xMoveA = 3.0f;
float tricolorA1 = -2.5f;
float tricolorA2 = -2.5f;
float rColorD1 = 0.0, gColorD1 =0.0f, bColorD1 = 0.0f;
float rColorD2 = 0.0, gColorD2 = 0.0f, bColorD2 = 0.0f;
bool flag = true;


// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);



// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("INDIA"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{		
						
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
				update();
				display();

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}
       

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;
	case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;  // change for 3d effect


	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	// for 3D
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// could not get what difference it makes observed same thing. Ask Sir??
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // to avoid distortion due to perspective view
	glShadeModel(GL_SMOOTH);  // to avoid alaizing
	
	resize(WINWIDTH, WINHEIGHT);
	// when I uncomment this and run the code, triagle is clipped at the vertices.
	// why this happens?? after toggleing the fullscreen, it looks fine.

}

void display(void) {


	void drawTriangle(float traiangleScale);
	void drawGraphPaper(void);
	void drawRectangle(float rectScale);
	void drawPyramid(void);
	void drawCube(void);
	void drawCharacter(byte character); // to print INDIA characters on screen
	
	//for (xMove = -3.0f; xMove <= -2.0f; xMove += 0.01f) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // GL_DEPTH_BUFFER_BIT - For 3D
	glMatrixMode(GL_MODELVIEW);
		
	glLoadIdentity();
	glTranslatef(xMoveI, 0.0f, -3.0f); // keep identical space between each two letters
	drawCharacter('I');

	glLoadIdentity();
	//glTranslatef(-1.25f, 0.0f, -3.0f);
	glTranslatef(-1.25f, yMoveN, -3.0f);
	drawCharacter('N');

	glLoadIdentity();
	glTranslatef(-0.25f, 0.0f, -3.0f);
	//glTranslatef(-0.25, yMoveI, -3.0f);
	drawCharacter('D');

	glLoadIdentity();
	glTranslatef(0.5f, yMoveI, -3.0f);
	drawCharacter('I');

	glLoadIdentity();
	//glTranslatef(1.5f, 0.0f, -3.0f);
	glTranslatef(xMoveA, 0.0f, -3.0f);
	drawCharacter('A');

	glLoadIdentity();
	//glTranslatef(1.5f, 0.0f, -3.0f);
	glTranslatef(0.0f, 0.0f, -3.0f);
	drawCharacter('-');

	SwapBuffers(ghdc);

}
// update rotation angles
void update(void) {

	if (xMoveI <= -2.0f) {
		xMoveI += 0.0002f;
		//xMove = -3.0f;
	}
	else if (yMoveN >= 0.0f) {
		yMoveN -= 0.001;
	}
	else if (rColorD1 <= 239.f) { // from black --> red --> orange
		
		rColorD1 += 0.1f;
		if (rColorD1 >= 200.0) {
			if (gColorD1 <= 70.0) {
				gColorD1 += 0.1f;
			}
			if (bColorD1 <= 10.0f) {
				bColorD1 += 0.1f;

			}
		}
		

	} 
	else if (gColorD2 <= 126.0) { // first reach the white shade then reduce the color shades to reach desired green color
		rColorD2 += 0.1;
		gColorD2 += 0.1;
		bColorD2 += 0.1;
		if (rColorD2 >= 122.0) {
			rColorD2 = 150.0;
			gColorD2 = 150.0;
			bColorD2 = 150.0;
		}
	}

	else if (bColorD2 >= 19.0f) { // desired green color

		bColorD2 -= 0.1f;
		if (gColorD2 >= 128.0) {

		gColorD2 -= 0.1f;
		}
		rColorD2 -= 0.1f;

	}
	else if (yMoveI <= 0.0f) {
		yMoveI += 0.001f;
	}
	else if (xMoveA >= 1.5f) {
		xMoveA -= 0.0005f;
	} 
	else if (tricolorA1 <= 1.5+0.25f) { // location adjustment for tricolor
		tricolorA1 += 0.001f;
	} 
	else if (tricolorA2 <= 1.5-0.25f) {
		tricolorA2 += 0.002;
	}
	
}


// draw Pyramid
void drawPyramid(void) {

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f); // front face left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f);// green

	glColor3f(0.0f, 0.0f, 1.0f); // front face 
	glVertex3f(1.0f, -1.0f,1.0f); // blue


	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // red appex


	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // red appex


	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // red appex


	glEnd();

}



//draw rectangle
void drawRectangle(float rectScale) {

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-rectScale, rectScale, 0.0f);
	glVertex3f(-rectScale, -rectScale, 0.0f);
	glVertex3f(rectScale, -rectScale, 0.0f);
	glVertex3f(rectScale, rectScale, 0.0f);
	glEnd();
}


// draws triangle
void drawCharacter(byte character) {


	switch (character) {

	case 'I':
		glLineWidth(4.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(0.0f, 0.95, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(0.0f, -0.95, 0.0f);
		glEnd();
		break;

	case 'N':
		glLineWidth(4.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(-0.25f, 0.95, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(-0.25f, -0.95, 0.0f);
		glEnd();
	
		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(0.25f, 0.95, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(0.25f, -0.95, 0.0f);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(-0.25f, 0.95, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(0.25f, -0.95, 0.0f);
		glEnd();
		break;

	case 'D':
		glLineWidth(4.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(rColorD1 / 255.0, gColorD1 / 255.0, bColorD1 / 255.0);
		glVertex3f(-0.28f, 0.95, 0.0f);
		glVertex3f(0.25f, 0.95, 0.0f);

		glEnd();

		glBegin(GL_LINE_LOOP);
		//glColor3f(236.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glColor3f(rColorD1 / 255.0, gColorD1 / 255.0, bColorD1 / 255.0);
		glVertex3f(-0.25f, 0.95, 0.0f);
		glColor3f(rColorD2/ 255.0, gColorD2 / 255.0, bColorD2 / 255.0);
		glVertex3f(-0.25f, -0.95, 0.0f);
		glEnd();

		glBegin(GL_LINE_LOOP);
		//glColor3f(236.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glColor3f(rColorD1 / 255.0, gColorD1 / 255.0, bColorD1 / 255.0);
		glVertex3f(0.25f, 0.95, 0.0f);
		glColor3f(rColorD2 / 255.0, gColorD2 / 255.0, bColorD2 / 255.0);
		glVertex3f(0.25f, -0.95, 0.0f);
		glEnd();

		glBegin(GL_LINE_LOOP);
		//glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glColor3f(rColorD2 / 255.0, gColorD2 / 255.0, bColorD2 / 255.0);
		glVertex3f(-0.28f, -0.95, 0.0f);		
		glVertex3f(0.25f, -0.95, 0.0f);
		glEnd();
		break;
		
	case 'A': 
		glLineWidth(4.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(0.0f, 0.95f, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(-0.5f, -0.95f, 0.0f);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		glVertex3f(0.0f, 0.95, 0.0f);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		glVertex3f(0.5f, -0.95, 0.0f);
		glEnd();
		break;

	case '-':  //tricolor horizontal line of A
		glLineWidth(3.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(239.0 / 255.0, 78.0 / 255.0, 10.0 / 255.0);
		//glVertex3f(0.245f, 0.008f, 0.0f);
		//glVertex3f(-0.245f, 0.008f, 0.0f);
		glVertex3f(tricolorA1-0.005, 0.009f, 0.0f);
		glVertex3f(tricolorA2+0.005, 0.009f, 0.0f);
		glEnd();

		glLineWidth(2.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(255.0, 255.0, 255.0);
		//glVertex3f(0.25f, 0.0f, 0.0f);
		//glVertex3f(-0.25f, 0.0f, 0.0f);
		glVertex3f(tricolorA1, 0.0f, 0.0f);
		glVertex3f(tricolorA2, 0.0f, 0.0f);
		glEnd();

		glLineWidth(3.0);
		glBegin(GL_LINE_LOOP);
		glColor3f(19.0 / 255.0, 128.0 / 255.0, 6.0 / 255.0);
		//glVertex3f(0.255f, -0.008, 0.0f);		
		//glVertex3f(-0.255f, -0.008, 0.0f);
		glVertex3f(tricolorA1+0.005, -0.009, 0.0f);
		glVertex3f(tricolorA2-0.005, -0.009, 0.0f);
		glEnd();	
		
		break;
		
	}

}


// draw graph paper

void drawGraphPaper(void) {

	float xIndex = 0.0f;
	float yIndex = 0.0f;
	glLineWidth(3.0f);
	//start drawing graph
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (xIndex = -0.95f; xIndex < 1.0f; (xIndex += 0.05f))
	{
		glVertex3f(xIndex, 1.0f, 0.0f);
		glVertex3f(xIndex, -1.0f, 0.0f);
	}


	for (yIndex = -0.95f; yIndex < 1.0f; (yIndex += 0.05f))
	{
		glVertex3f(1.0f, yIndex, 0.0f);
		glVertex3f(-1.0f, yIndex, 0.0f);
	}
	glEnd();
	// end of graph


}



void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	//glFrustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 10.0f);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	/* Queries :
		1. How can we see the object with FOV angle zero degrees??
		2. Why glTranslate doesn't work when FOV is zero?
		   and why it works when FOV is 45?
	*/
	//gluPerspective(0.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	DestroyWindow(ghwnd);
}