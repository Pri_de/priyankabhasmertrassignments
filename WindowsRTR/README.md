My RTR Assignment's README

4 Assignments given on 29th July 2017.
-The purpose of these assignments is to create and transform 3D objects, such as Pyramid and cube.
-glRotate is used to rotate pyramid accross y-axis and cube across arbitrary axis(all x,y,z)
-OpenGLDrawNRotateCubeUsingMatrices assignment stands out since this is not done with the conventional ways,
e.g. glLoadIdentity(), glTranlatef(), glRotate() functions completely eliminated. Here we have defined every Matrix explicitily
	- each matrix is of 16 members
	- glLoadMatrix() and glMatrixMult() is used to perform glLoadIdentity() and glTranlatef(), glRotate() operations respectively
	- This assignment brings us slight glimpse of Programmable Function Pipeline

-Enjoy :)
	
