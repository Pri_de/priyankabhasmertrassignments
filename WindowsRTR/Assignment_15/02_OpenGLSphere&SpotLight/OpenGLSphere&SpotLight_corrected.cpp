// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// Pragma
#pragma comment(lib, "opengl32.lib")
// we need this to link
#pragma comment(lib, "glu32.lib")


// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898


float gwSideofTriangle = 2.0f;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone, gbLightingFlag = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

float redLightAngle, greenLightAngle, blueLightAngle;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLUquadric *quadric = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Lighting
GLboolean gbPyramid = GL_TRUE;
GLboolean gbCube = GL_FALSE;
GLboolean gbSphere = GL_FALSE;

//Light0
GLfloat red_light0_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_light0_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f }; //{1.0f, 0.0f, 0.0f, 0.0f };
GLfloat red_light0_specular[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat red_light0_position[] = {0.0f, 0.0f, 1.0f, 0.0f };

//Light1
GLfloat blue_light1_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat blue_light1_diffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light1_specular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat blue_light1_position[] = { 0.0f, 1.0f, 1.0f, 1.0f };
GLfloat blue_light1_direction[] = { 0.0f,0.0f,-1.0f, 0.0f };

//material
GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };//{ 0.0f, 0.0f, 1.0f, 0.0f }; //{ 0.5f, 0.5f, 0.5f, 0.5f }; //{ 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shyniness[] = {50.0};

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("PBB"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{		
						
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
				update();
				display();

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}
       

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;
	case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;

		case 'L':
			if (gbLightingFlag == false) {
				gbLightingFlag = true;
				glEnable(GL_LIGHTING);
			}
			else {
				gbLightingFlag = false;
				glDisable(GL_LIGHTING);
			}

			break;

		case 'P':
			gbPyramid = GL_TRUE;
			gbCube = GL_FALSE;
			gbSphere = GL_FALSE;
			break;

		case 'C':
			gbPyramid = GL_FALSE;
			gbCube = GL_TRUE;
			gbSphere = GL_FALSE;
			break;

		case 'S':
			gbPyramid = GL_FALSE;
			gbCube = GL_FALSE;
			gbSphere = GL_TRUE;
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;  // change for 3d effect


	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, red_light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, red_light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, red_light0_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, red_light0_position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, blue_light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, blue_light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, blue_light1_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, blue_light1_position);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, blue_light1_direction);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0f);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 0.2f);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.1f);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.2f);
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.5f);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shyniness);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);

	quadric = gluNewQuadric(); // for shpere
	
	// for 3D
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// could not get what difference it makes observed same thing. Ask Sir??
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // to avoid distortion due to perspective view
	glShadeModel(GL_SMOOTH);  // to avoid alaizing
	
	resize(WINWIDTH, WINHEIGHT);
}

void display(void) {

		
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // GL_DEPTH_BUFFER_BIT - For 3D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -4.0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);		
	gluSphere(quadric, 0.75, 30, 30);
	
	SwapBuffers(ghdc);

}
// update rotation angles
void update(void) {

	//regLightAngle, greenLightAngle, blueLightAngle;
	redLightAngle += 0.1f;
	if (redLightAngle >= 360) {
		redLightAngle = 0;
	}

	greenLightAngle += 1.0f;
	if (greenLightAngle >= 360) {
		greenLightAngle = 0;
	}

	blueLightAngle += 1.0f;
	if (blueLightAngle >= 360) {
		blueLightAngle = 0;
	}

}

void drawCube(void) {

	glBegin(GL_QUADS);

	//top face
	glColor3f(1.0f, 1.0f, 1.0f);
	glNormal3f(0.0f, 1.0f, 0.0f); //for light
	glVertex3f(1.0f, 1.0f, -1.0f); // anti clockwise from top right corner
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//bottom face
	glColor3f(0.0f, 1.0f, 0.0f);
	glNormal3f(0.0f, -1.0f, 0.0);
	glVertex3f(1.0f, -1.0f, -1.0f); // anti clockwise from bottom right corner
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//front face
	glColor3f(0.0f, 0.0f, 1.0f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); // anti clockwise from front right corner
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	//back face
	glColor3f(1.0f, 0.0f, 1.0f);
	glNormal3f(0.0f, 0.0f, -1.0);
	glVertex3f(-1.0f, 1.0f, -1.0f); // anti clockwise from back right corner
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	//right face
	glColor3f(0.0f, 1.0f, 1.0f);
	glNormal3f(1.0f, 0.0f, 0.0);
	glVertex3f(1.0f, 1.0f, -1.0f); // anti clockwise from right right corner
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//left face
	glColor3f(1.0f, 1.0f, 0.0f);
	glNormal3f(-1.0f, 0.0f, 0.0);
	glVertex3f(-1.0f, 1.0f, 1.0f); // anti clockwise from left right corner
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glEnd();
}

// draw Pyramid
void drawPyramid(void) {

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glNormal3f(0.0f, 0.0f, 1.0);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f); // front face left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f);// green

	glColor3f(0.0f, 0.0f, 1.0f); // front face 
	glVertex3f(1.0f, -1.0f, 1.0f); // blue

	glNormal3f(1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // red appex

	glNormal3f(0.0f, 0.0f, -1.0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // red appex

	glNormal3f(-1.0f, 0.0f, 0.0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // red appex

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // red appex

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // red appex


	glEnd();

}

void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	if (quadric != NULL) {
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	DestroyWindow(ghwnd);
}