// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>
#include "Materials.h"

// Pragma
#pragma comment(lib, "opengl32.lib")
// we need this to link
#pragma comment(lib, "glu32.lib")


// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898


float gwSideofTriangle = 2.0f;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone, gbLightingFlag = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

float lightAngle, x = 1.0f,y,z;

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLUquadric *quadric = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Lighting
GLboolean gbPyramid = GL_TRUE;
GLboolean gbCube = GL_FALSE;
GLboolean gbSphere = GL_FALSE;

//Light0
GLfloat light0_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; //{1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light0_position[] = { 0.0f, 0.0f, 0.0f, 0.0f };

//enum
typedef enum {
	FIRST_ROW = 1,
	SECOND_ROW,
	THIRD_ROW,
	FORTH_ROW
}rowNum;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("PBB"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{		
						
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
				update();
				display();

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}
       

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;
	case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;

		case 'L':
			if (gbLightingFlag == false) {
				gbLightingFlag = true;
				glEnable(GL_LIGHTING);
			}
			else {
				gbLightingFlag = false;
				glDisable(GL_LIGHTING);
			}

			break;

		case 'X':
			x = 1.0f;
			y = 0.0f;
			z = 0.0f;
			break;

		case 'Y':
			x = 0.0f;
			y = 1.0f;
			z = 0.0f;
			break;

		case 'Z':
			x = 0.0f;
			y = 0.0f;
			z = 1.0f;
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;  // change for 3d effect


	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
	//glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

	glEnable(GL_LIGHT0);


	quadric = gluNewQuadric(); // for shpere
	
	// for 3D
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// could not get what difference it makes observed same thing. Ask Sir??
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // to avoid distortion due to perspective view
	glShadeModel(GL_SMOOTH);  // to avoid alaizing
	
	resize(WINWIDTH, WINHEIGHT);
}

void display(void) {

	void setMaterialProperties(MaterialProperties material);
	float xPos = -3.0f;
	float yPos = 5.0f;
	static int sphereCounter = 0;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // GL_DEPTH_BUFFER_BIT - For 3D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	
	glPushMatrix();
	glRotatef(lightAngle, x, y, z);
	light0_position[(int)x] = lightAngle; // 1.0; also works
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	light0_position[(int)x] = 0.0f;
	glPopMatrix();
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	sphereCounter = 0;
	for (int column = 0; column < 4; column++) {
					
			for (int row = 0; row < 6; row++) {

			glPushMatrix();
			glTranslatef(xPos, yPos, -15.0);
			setMaterialProperties(gstMaterialProperties[sphereCounter++]);
			gluSphere(quadric, 0.75, 30, 30);
			glPopMatrix();
			yPos -= 2.0f;
		}
			xPos += 2.0f;
			yPos = 5.0f;

	}

	//drawSphere(THIRD_ROW);

	

	glPopMatrix();
	SwapBuffers(ghdc);

}

// update rotation angles
void update(void) {

	//regLightAngle, greenLightAngle, blueLightAngle;
	lightAngle += 0.1f;
	if (lightAngle >= 360) {
		lightAngle = 0;
	}
}

void setMaterialProperties(MaterialProperties material)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffused);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material.specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material.shininess);
}


void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	if (quadric != NULL) {
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	DestroyWindow(ghwnd);
}