/* Priyanka Bhasme
**Native assignment # 6
** Draw the symbol of rotating 'The Deathly Hallows' in white color.
Output - rorating 'The Deathly Hallows'
By changing co-ordinates of traingle output could be changed.
Used generic equations of triangle and inscribed circle.
*/

// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// Pragma
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898

float gwSideofTriangle = 2.0f;//sqrt(3);
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
// ask sir - how error goes when curly brackets are added????????????
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

float rotateAngle;



// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);



// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("PBB"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}
       

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;
	case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WINWIDTH, WINHEIGHT);

}


// update rotation angle
void update(void) {
	
	rotateAngle += 0.1f;
	if (rotateAngle >= 360.0f) {
		rotateAngle = 0.0f;
	}
}

// display rotating deathly hallows
void display(void) {

	//prototype
	void drawTriangle(float ax, float ay, float bx, float by, float cx, float cy);

	float xIndex = 0.05f;
	float yIndex = 0.0f;
	float angle = 0.0f;
	float cosAngle = 0.0f;
	float sinAngle = 0.0f;

	/* for testing please change the co-ordinates here --------------- */
	float Ax = 0.0f;
	float Ay = 1.0f;
	float Bx = -1.0f;
	float By = -1.0f;
	float Cx = 1.0f;
	float Cy = -1.0f;

	float lengthAB;
	float lengthBC;
	float lengthCA;

	float perimeter;
	float semiperimeter;
	float area;
	float radiusOfInscribedCircle;

	// co-ordinates of the center of the inscribed circle
	float xIncenter;
	float yIncenter;


	/* equation for circumscribed circle is -> r = Area / semiperimeter of the traingle
	 hence calculate perimeter of the traingle first */

	// using equation of line
	lengthAB = sqrt(pow(Bx - Ax, 2) + pow(By - Ay, 2));
	lengthBC = sqrt(pow(Cx - Bx, 2) + pow(Cy - By, 2));
	lengthCA = sqrt(pow(Ax - Cx, 2) + pow(Ay - Cy, 2));

	//calculate semiperimeter
	perimeter = (lengthAB + lengthBC + lengthCA);
	semiperimeter = perimeter / 2;
	
	//calculate area of traingle
	area = sqrt(semiperimeter *
		(semiperimeter - lengthAB) *
		(semiperimeter - lengthBC) *
		(semiperimeter - lengthCA));

	//calculate radias of inscribed circle
	radiusOfInscribedCircle = area / semiperimeter;

	//calculate incenter co-ordinates
	xIncenter = ((Ax * lengthBC) + (Bx * lengthCA) + (Cx * lengthAB)) / perimeter;
	yIncenter = ((Ay * lengthBC) + (By * lengthCA) + (Cy * lengthAB)) / perimeter;


	//OpenGL code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glTranslatef(0.0f, 0.0f, -4.0f);

	// Draw verticle line, no need to rotate
	glBegin(GL_LINES);
	glVertex3f(Ax, Ay, 0.0f);
	glVertex3f((Bx + Cx) / 2, (By + Cy) / 2, 0.0f);
	glEnd();

	glRotatef(rotateAngle, 0.0f, 1.0f, 0.0f);
	// draw triangle with the co-ordinates
	drawTriangle(Ax, Ay, Bx, By, Cx, Cy);
	
	// Draw incsribed circle
	glPointSize(3.0f);
	glBegin(GL_POINTS);
	
	for (int i = 0; i < 1000; i++) {

		angle = 2 * PI * i / 1000.0f; // ;

		/* Specific for standard bilateral triangle and inscribed circle*/
		//cosAngle = cos(angle) * ((sqrt((2 * sqrt(5) - 2.0f) / (2 * sqrt(5) + 2.0f)) / 2) * gwSideofTriangle);
		//sinAngle = sin(angle) * ((sqrt((2 * sqrt(5) - 2.0f) / (2 * sqrt(5) + 2.0f)) / 2) * gwSideofTriangle) - (1 - (sqrt((2 * sqrt(5) - 2.0f) / (2 * sqrt(5) + 2.0f))));//0.4823619097f;//(((sqrt(3) / 6) * gwSideofTriangle)/6) ;

		/* for genric inscribed circle */
		cosAngle = cos(angle) * radiusOfInscribedCircle;
		sinAngle = sin(angle) * radiusOfInscribedCircle;
		glVertex2f(cosAngle + xIncenter, sinAngle + yIncenter);

	}

	glEnd();

	SwapBuffers(ghdc);

}

void drawTriangle(float ax, float ay, float bx, float by, float cx, float cy) {
	
	glLineWidth(3.0f);
	// Draw Triagle	
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(ax, ay, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(bx, by, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(cx, cy, 0.0f);
	glEnd();
}


void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (GLfloat)width / (GLfloat)height, 1, 100);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	DestroyWindow(ghwnd);
}